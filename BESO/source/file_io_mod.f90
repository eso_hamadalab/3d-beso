module FILE_IO
  use BESO_SUPER
  use BESO_DATA
  use MODEL_MOD
  use MODEL_SUPER
  use PHYSICAL_DATA
  implicit none
contains

!-------------------------------------------------------------------------------
! subroutine Time_Stamp
! 解析時間・CPU時間などの記録
!-------------------------------------------------------------------------------
subroutine Time_Stamp(message)
  use BESO_DATA
  implicit none
  character(*), intent(in) :: message

  character :: dateStr*8, timeStr*10
  real(8) :: cpuTime
  integer :: txtLen, diff_tf, t_max
  integer :: tf = 0, tr = 0

  integer, save :: call_first = 1, pre_tf = 0
  character, save :: pre_dateStr, pre_timeStr
  real(8), save :: pre_cpuTime=0.d0, pre_realTime

  txtLen = len(message)
  call date_and_time(dateStr, timeStr)

  if (call_first == 1) then
    call_first = 0
    open(200, file='./dataout/overview/time_stamp.txt', action='write')
    ! Header
    write(200,fmt='("Date",8x,"Time",10x,"CPU_Time",7x,"Diff_Time",6x,"Diff_CPU_Time",5x,"Event")')

    ! 時間計測
    call cpu_time(cpuTime)
    call system_clock(tf)
    diff_tf = tf - pre_tf

  else
    open(200, file='./dataout/overview/time_stamp.txt', action='write', access='append')

    ! 時間計測
    call cpu_time(cpuTime)
    call system_clock(tf, tr, t_max)
    if ( tf < pre_tf ) then
      diff_tf = (t_max - pre_tf) + tf + 1
    else
      diff_tf = tf - pre_tf
    endif

  end if

  ! Date
  write(200,fmt='(a4,"/",a2,"/",a2,2x)', advance='no') dateStr(1:4), dateStr(5:6), dateStr(7:)
  ! Time
  write(200,fmt='(a2,":",a2,":",a6,2x)', advance='no') timeStr(1:2), timeStr(3:4), timeStr(5:)
  ! CPU Time
  write(200,fmt='(e15.7,2x)', advance='no') cpuTime
  ! Real diff
  write(200,fmt='(f10.4,2x)', advance='no') diff_tf/dble(tr)
  ! CPU diff
  write(200,fmt='(f10.4,2x)', advance='no') cpuTime - pre_cpuTime
  ! Event
  write(200,fmt='(a1,a<txtLen>,a1)') '"', message, '"'

  pre_tf      = tf
  pre_cpuTime = cpuTime
  pre_timeStr = timeStr
  pre_dateStr = pre_dateStr
  close(200)
end subroutine Time_Stamp

!-------------------------------------------------------------------------------
! subroutine OutputOverView
!   output calculation overview.
!-------------------------------------------------------------------------------
subroutine OutputOverView
  use BESO_SUPER
  use BESO_DATA
	implicit none
  integer :: str_len
  str_len = len(modelName)
	open(200,file='./dataout/overview/overview.txt', action='write')
	write(200,fmt='("モデル名:",2x,a<str_len>)') modelName
	write(200,fmt='("現ステップ:",2x,i3)')       kstep
	write(200,fmt='("最終ステップ:",2x,i3)')     step_end
  ! 解析終了の確認
  if (kstep<step_end) then
  	write(200,fmt='("計算状態:",2x,a10)')     'incomplete'
  else
  	write(200,fmt='("計算状態:",2x,a10)')     'complete'
  end if
  ! 最適化対象
  if (opt_target==1) then
  	write(200,fmt='("最適化:",2x,a10)')      'Mises'
  elseif (opt_target==2) then
    write(200,fmt='("最適化:",2x,a10)')      'dispSense'
  else
    write(200,fmt='("最適化:",2x,a10)')      'buckring'
  end if
	write(200,fmt='("位相履歴:",2x,i3)')        history
	close(200)
end subroutine OutputOverView

!-------------------------------------------------------------------------------
! subroutine InputOverView
!   input calculation overview.
!-------------------------------------------------------------------------------
subroutine InputOverView
	implicit none
	integer :: access
	character :: tmp_str, buff_str*100
  character(:), allocatable :: in_modelName

	integer :: in_kstep, in_endStep, in_topoHistory
  integer :: in_calc_status, in_opt_target

	! 0が返ってきたら存在している
	if (access('./overview.txt','r')/=0) return

	open(200,file='./overview.txt',action='read')
	read(200,*) tmp_str, buff_str
  in_modelName = trim(buff_str)
	read(200,*) tmp_str, in_kstep
	read(200,*) tmp_str, in_endStep

  ! read calculation status.
	read(200,*) tmp_str, buff_str
  if (buff_str(1:1)=='i') then
    in_calc_status = 0
  elseif (buff_str(1:1)=='c') then
    in_calc_status = 1
  end if

  ! read optimization target.
	read(200,*) tmp_str, buff_str
  if (buff_str(1:1)=='M') then
    in_opt_target = 1
  elseif (buff_str(1:1)=='d') then
    in_opt_target = 2
  elseif (buff_str(1:1)=='b') then
    in_opt_target = 3
  end if

	read(200,*) tmp_str, in_topoHistory

end subroutine InputOverView

!-------------------------------------------------------------------------------
! function AllocInputter
! 入力を受け取り、入力された文字長さの文字を返す。
!-------------------------------------------------------------------------------
function AllocInputter()
  character(:), allocatable :: AllocInputter
  character(100) :: input_char

  read(*,*) input_char
  AllocInputter = trim(input_char)

end function

!-------------------------------------------------------------------------------
! subroutine OutputAnalysis_Summary
! ステップごとの体積などを出力
!-------------------------------------------------------------------------------
subroutine OutputAnalysis_Summary
  use BESO_SUPER
  use BESO_DATA
	implicit none

	if (kstep == 1) then
    open(300, file='./dataout/overview/' // modelName // '_' // 'BESO_summary.txt', action='write')
		write(300,fmt='(5(A15,2X))') 'step', 'volume', 'ElmNum', 'volume-rate', 'criteria-val'
  else
		open(300, file='./dataout/overview/' // modelName // '_' // 'BESO_summary.txt', position='append', action='write')
	end if

	write(300, fmt='(I15, 2X, E15.7, 2X, I15, 2X, E15.7, 2X, E15.7)') kstep, Hx*Hy*Hz*dble(El1), El1, dble(El1)/dble(El), Vp0
	close(300)

end subroutine OutputAnalysis_Summary

!-------------------------------------------------------------------------------
! subroutine OutputModel_info
! モデル情報のアウトプット
!-------------------------------------------------------------------------------
subroutine OutputModel_info
  use BESO_SUPER
  use BESO_DATA
  implicit none
  integer :: i

  open(200, file='./dataout/topology_info/' // modelName // '_' // kstepStr // '_model.dat', action='write')
  write(200,*) '# element num'
  write(200,fmt='(I10)') El
  write(200,fmt='(3(A12,2X),A15)') 'Be', 'Cremap', 'material_ID', 'SenseHistry'
  do i = 1, El
    write(200,fmt='(3(I12,2X),E15.7)') Be(i), Cre_Map(i), material_map(i), Elsen_hist(i)
  end do
  close(200)

end subroutine OutputModel_info

!-------------------------------------------------------------------------------
! subroutine OutputBESO_Parm
! BESO法のパラメーターのアウトプット
!-------------------------------------------------------------------------------
subroutine OutputBESO_Parm
  use BESO_SUPER, modelName_in => modelName
  use BESO_DATA
  use MODEL_MOD
  use PHYSICAL_DATA
  implicit none
  integer :: int_vec(3)
  real(8) :: dble_vec(3)
  character :: strID*2
  integer :: i, j

  NAMELIST/ BESO_PARAM/ modelName_in, coeff_von, coeff_disp, coeff_buck, coeff_daylight, &
    & step_start, step_end, target_rate, e_rate, topo_rate, symType, history, &
    & r_min, filter_degree, Bsen_mixPenalty, BsenJoinNum, buckAnalyNum
  NAMELIST/ Build_PARAM/ Xn, Yn, Zn, Nx, Ny, Nz
  NAMELIST/ PhysicNum_PARAM/ mtNum
  NAMELIST/ Physic_PARAM/ young_list, poisson_list, density_list, gravityDir, grav_weight

  ! BESO parameter
  open(200, file='./dataout/topology_info/' // modelName_in // '_beso_param.dat', action='write')
  write(200, NML=BESO_PARAM)
  close(200)

  ! physical setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_physic.dat', action='write')
  write(200,NML=PhysicNum_PARAM)
  write(200,NML=Physic_PARAM)
  close(200)

  ! build area setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_build.dat', action='write')
  write(200, NML=Build_PARAM)
  close(200)

  ! load setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_load.dat', action='write')
  write(200,*) '# case num'
  write(200,fmt='(I5)') load_caseNum
  do i = 1, load_caseNum
    write(strID,fmt='(I2.2)') i
    ! 各ケースデータ長
    write(200,*) '# case' // strID // ' load listLen'
    write(200,fmt='(I5)') load_data(i)%list_len
    ! 各方向荷重データ数
    write(200,*) '# case' // strID // ' load num'
    write(200,fmt='(3(I15,2X))') load_data(i)%Num_vec(:)
    ! 各方向荷重節点ID
    write(200,*) '# case' // strID // ' load node'
    do j = 1, load_data(i)%list_len
      write(200,fmt='(3(I15,2X))') load_data(i)%ID_vec(j,:)
    end do
    ! 各方向節点荷重値
    write(200,*) '# case' // strID // ' load value'
    do j = 1, load_data(i)%list_len
      write(200,fmt='(3(E15.7,2X))') load_data(i)%Val_vec(j,:)
    end do
  end do
  close(200)

  ! support setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_support.dat', action='write')
  write(200,*) '# case num'
  write(200,fmt='(I5)') support_caseNum
  write(200,*) '# support_listLen'
  write(200,fmt='(I5)') support_listLen
  do i = 1, support_caseNum
    write(strID,fmt='(I2.2)') i
    write(200,*) '# case' // strID // ' support num'
    write(200,fmt='(3(I15,2X))') NFIX_1(:)
    write(200,*) '# case' // strID // ' support node'
    do j = 1, maxval(NFIX_1(:))
      write(200,fmt='(3(I15,2X))') NFIXP_1(j,:)
    end do
  end do
  close(200)

  ! analysis setting
  open(200, file='./dataout/topology_info/' // modelName_in // '_analy.dat', action='write')
  write(200,*) '# analysis case num'
  write(200,fmt='(I5)') totalAnalyCase
  write(200,fmt='(3(A15,2X))') 'caseID', 'loadID', 'supportID'
  do i = 1, totalAnalyCase
    write(200,fmt='(3(I15,2X))') i, analyCase_list(i,:)
  end do
  close(200)

end subroutine OutputBESO_Parm

!-------------------------------------------------------------------------------
! subroutine output_vtkfile_ug
! vtk out test
!-------------------------------------------------------------------------------
subroutine output_vtkfile_ug
  integer :: file_id
  character :: char_num*3
  integer :: i

  file_id = 4
  open(file_id, file='dataout/analysis/'// modelName // '_' // kstepStr // 'c' // analyCaseStr // '.vtk' &
    &, form='binary', convert='BIG_ENDIAN')

  call binary_vtk_out_ug(file_id, 'test', MDL_nodeNum, transpose(MDL_nodePos(:,:)), &
    & MDL_elNum, transpose(indv(:,:)), 12)

  ! cell data
  call binary_label(file_id, 'CELL_DATA', MDL_elNum)

  call binary_scalar(file_id, 'Mises', MDL_vonMises(:))
  call binary_scalar(file_id, 'Disp_sense', MDL_dispSense(:))

  do i = 1, MDL_BsenJoinNum
    write(char_num,'(i3.3)') i
    call binary_scalar(file_id, 'Buck_sense' // char_num , MDL_buckSense_list(:,i))
  end do

  call binary_scalar(file_id, 'sigma-x', stress(:,1))
  call binary_scalar(file_id, 'sigma-y', stress(:,2))
  call binary_scalar(file_id, 'sigma-z', stress(:,3))
  call binary_scalar(file_id, 'sigma-xy', stress(:,4))
  call binary_scalar(file_id, 'sigma-xz', stress(:,5))
  call binary_scalar(file_id, 'sigma-yz', stress(:,6))

  ! point data
  call binary_label(file_id, 'POINT_DATA', MDL_nodeNum)
  call binary_vector(file_id, 'displacement', reshape(disp(:), (/3,MDL_nodeNum/)) )
  do i = 1, eigenSolveNum
    write(char_num,'(i3.3)') i
    call binary_vector(file_id, 'mode_vec' // char_num, reshape(mode_vectors(:,i), (/3,MDL_nodeNum/)) )
  end do

  close(file_id)

end subroutine output_vtkfile_ug

!-------------------------------------------------------------------------------
! subroutine output_vtkfile
! vtk out test
!-------------------------------------------------------------------------------
subroutine output_vtkfile_ps
  use MODEL_SUPER
  integer :: file_id
  file_id = 4
  open(file_id, file='dataout/build_domain/' // modelName // '_' // kstepStr // 'build.vtk' &
  & , form='binary', convert='BIG_ENDIAN')

  call binary_vtk_out_ps(file_id, 'test', (/nx+1, ny+1, nz+1/), (/0.d0, 0.d0, 0.d0/), &
  & (/hx, hy, hz/))

  call binary_label(file_id, 'CELL_DATA', El)

  call binary_scalar(file_id, 'Sense_Origin', Sensitivity(:))
  call binary_scalar(file_id, 'Sense_Filtered', El_sen(:))
  call binary_scalar(file_id, 'diff_sense', diff_sense(:))

  close(file_id)

end subroutine output_vtkfile_ps

! structured points
subroutine ascii_vtk_out_ps(file_id, data_name, dim_vec, origin_vec, space_vec)
  implicit none
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_name
  integer, intent(in) :: dim_vec(3)
  real(8), intent(in) :: origin_vec(3)
  real(8), intent(in) :: space_vec(3)

  ! character :: vtk_header*30, file_format*6
  ! character :: dataset_structure*30
  integer :: cell_node_num
  character :: int_char*12
  integer :: i

  ! file header
  write(file_id,'(a)') '# vtk DataFile Version 2.0'
  write(file_id,'(a)') data_name
  write(file_id,'(a)') 'ASCII'
  write(file_id,'(a)') 'DATASET STRUCTURED_POINTS'
  write(file_id,'(a, 3(1X,I12))') 'DIMENSIONS', dim_vec(1:3)
  write(file_id,'(a, 3(1X,E15.7))') 'ORIGIN', origin_vec(1:3)
  write(file_id,'(a, 3(1X,E15.7))') 'SPACING', space_vec(1:3)

end subroutine ascii_vtk_out_ps

! structured points
subroutine binary_vtk_out_ps(file_id, data_name, dim_vec, origin_vec, space_vec)
  implicit none
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_name
  integer, intent(in) :: dim_vec(3)
  real(8), intent(in) :: origin_vec(3)
  real(8), intent(in) :: space_vec(3)

  character :: int_char*15
  character :: lf*1
  integer :: i

  lf = char(10)

  ! file header
  write(file_id) '# vtk DataFile Version 2.0' // lf
  write(file_id) data_name // lf
  write(file_id) 'BINARY' // lf
  write(file_id) 'DATASET STRUCTURED_POINTS' // lf

  write(file_id) 'DIMENSIONS'
  do i = 1, 3
    write(int_char,'(I12)')  dim_vec(i)
    write(file_id) ' ' // trim(adjustl(int_char))
  end do
  write(file_id) lf

  write(file_id) 'ORIGIN'
  do i = 1, 3
    write(int_char,'(E15.7)')  origin_vec(i)
    write(file_id) ' ' // trim(adjustl(int_char))
  end do
  write(file_id) lf

  write(file_id) 'SPACING'
  do i = 1, 3
    write(int_char,'(E15.7)')  space_vec(i)
    write(file_id) ' ' // trim(adjustl(int_char))
  end do
  write(file_id) lf

end subroutine binary_vtk_out_ps

! unstructured grid
subroutine ascii_vtk_out_ug(file_id, data_name, pt_num, pt_list, cell_num, cell_list, cell_type)
  implicit none
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_name
  integer, intent(in) :: pt_num
  real(8), intent(in) :: pt_list(:,:) ! pt_list(3,pt_num)

  integer, intent(in) :: cell_num
  integer, intent(in) :: cell_list(:,:) ! cell_list(:,cell_num)
  integer, intent(in) :: cell_type

  ! character :: vtk_header*30, file_format*6
  ! character :: dataset_structure*30
  integer :: cell_node_num
  character :: int_char*12
  integer :: i

  ! vtk_header = '# vtk DataFile Version 2.0'
  ! file_format = 'ASCII'
  ! dataset_structure = 'DATASET UNSTRUCTURED_GRID'

  ! file header
  write(file_id,'(a)') '# vtk DataFile Version 2.0'
  write(file_id,'(a)') data_name
  write(file_id,'(a)') 'ASCII'
  write(file_id,'(a)') 'DATASET UNSTRUCTURED_GRID'

  ! point info
  write(int_char,'(I12)') pt_num
  write(file_id,'(a)') 'POINTS ' // trim(int_char) // ' double'
  do i = 1, pt_num
    write(file_id,'(3(E15.7,1X))') pt_list(:,i)
  end do

  ! cell info
  cell_node_num = size(cell_list, dim=1)
  write(file_id,'(a,1X)',advance='no') 'CELLS'
  write(int_char,'(I12)') cell_num
  write(file_id,'(a,1X)',advance='no') trim(int_char)
  write(int_char,'(I12)') (cell_node_num + 1) * cell_num
  write(file_id,'(a)') trim(int_char)
  do i = 1, cell_num
    write(file_id,'(<cell_node_num + 1>(I12,1X))') cell_node_num, cell_list(:,i) - 1
  end do

  ! cell type
  write(int_char,'(I12)') cell_num
  write(file_id,'(a,1X)',advance='no') 'CELL_TYPES'
  write(file_id,'(a)') trim(int_char)
  write(int_char,'(I12)') cell_type
  do i = 1, cell_num
    write(file_id,'(a,1X)') trim(int_char)
  end do

end subroutine ascii_vtk_out_ug

subroutine ascii_scalar(file_id, data_name, data)
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_name
  real(8), intent(in) :: data(:)

  integer :: data_num
  integer :: i

  data_num = size(data(:))
  write(file_id,'(a)') 'SCALARS ' // data_name // ' double'
  write(file_id,'(a)') 'LOOKUP_TABLE default'
  do i = 1, data_num - 1
    write(file_id,'(E15.7,1X)',advance='no') data(i)
  end do
  write(file_id,'(E15.7)') data(data_num)

end subroutine ascii_scalar

subroutine ascii_vector(file_id, data_name, data)
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_name
  real(8), intent(in) :: data(:,:) ! data(3,data_num)

  integer :: data_num
  integer :: i

  data_num = size(data(1,:))
  write(file_id,'(a)') 'VECTORS ' // data_name // ' double'
  do i = 1, data_num
    write(file_id,'(3E15.7)') data(:,i)
  end do

end subroutine ascii_vector

!---------------------
! ascii data label
! vvv Input vvv
! file_id -> unit number
! data_label ->  e.g.'CELL_DATA'
! data_num -> component number
subroutine ascii_label(file_id, data_label, data_num)
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_label
  integer, intent(in) :: data_num

  character :: int_char*12
  character :: lf*1
  lf = char(10)

  write(int_char,'(I12)') data_num
  write(file_id,'(a)') data_label // ' ' // trim(int_char)

end subroutine ascii_label

! unstructured grid
subroutine binary_vtk_out_ug(file_id, data_name, pt_num, pt_list, cell_num, cell_list, cell_type)
  implicit none
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_name
  integer, intent(in) :: pt_num
  real(8), intent(in) :: pt_list(:,:) ! pt_list(3,pt_num)

  integer, intent(in) :: cell_num
  integer, intent(in) :: cell_list(:,:) ! cell_list(:,cell_num)
  integer, intent(in) :: cell_type

  integer :: cell_node_num
  character :: int_char*12
  character :: lf*1
  integer :: i

  lf = char(10)

  ! file header
  write(file_id) '# vtk DataFile Version 2.0' // lf
  write(file_id) data_name // lf
  write(file_id) 'BINARY' // lf
  write(file_id) 'DATASET UNSTRUCTURED_GRID' // lf

  ! point info
  write(int_char,'(I12)') pt_num
  write(file_id) 'POINTS ' // trim(int_char) // ' double' // lf
  write(file_id) pt_list(:,:)
  write(file_id) lf

  ! cell info
  cell_node_num = size(cell_list, dim=1)
  write(file_id) 'CELLS '
  write(int_char,'(I12)') cell_num
  write(file_id) trim(int_char) // ' '
  write(int_char,'(I12)') (cell_node_num + 1) * cell_num
  write(file_id) trim(int_char) // lf
  do i = 1, cell_num
    write(file_id) cell_node_num, cell_list(:,i) - 1
  end do
  write(file_id) lf

  ! cell type
  write(file_id) 'CELL_TYPES '
  write(int_char,'(I12)') cell_num
  write(file_id) trim(int_char) // lf
  do i = 1, cell_num
    write(file_id) cell_type
  end do
  write(file_id) lf

end subroutine binary_vtk_out_ug

subroutine binary_scalar(file_id, data_name, data)
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_name
  real(8), intent(in) :: data(:)

  character :: lf*1
  lf = char(10)

  write(file_id) 'SCALARS ' // data_name // ' double' // lf
  write(file_id) 'LOOKUP_TABLE default' // lf
  write(file_id) data(:)
  write(file_id) lf

end subroutine binary_scalar

subroutine binary_vector(file_id, data_name, data)
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_name
  real(8), intent(in) :: data(:,:) ! data(3,data_num)

  character :: lf*1
  lf = char(10)

  write(file_id) 'VECTORS ' // data_name // ' double' // lf
  write(file_id) data(:,:)
  write(file_id) lf

end subroutine binary_vector

!---------------------
! binary data label
! vvv Input vvv
! file_id -> unit number
! data_label ->  e.g.'CELL_DATA'
! data_num -> component number
subroutine binary_label(file_id, data_label, data_num)
  integer, intent(in) :: file_id
  character(*), intent(in) :: data_label
  integer, intent(in) :: data_num

  character :: int_char*12
  character :: lf*1
  lf = char(10)

  write(int_char,'(I12)') data_num
  write(file_id) data_label // ' ' // trim(int_char) // lf

end subroutine binary_label

end module FILE_IO
