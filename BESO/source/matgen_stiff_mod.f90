!===============================================================================
! module StiffMat_GenMOD
!	有限要素法マトリクスの生成
!===============================================================================
module StiffMat_GenMOD
  use MODEL_MOD
  implicit none

  real(8), parameter :: pi = 4.0d0 * datan(1.0d0)
  real(8), dimension(6,6) :: Dmatrix
  real(8), dimension(6,24) :: Bmatrix
  real(8), dimension(24,24) :: em

contains

!===============================================================================
! subroutine mkSkyStiff_Mat
!	要素剛性マトリクスの重ね合わせ
!===============================================================================
subroutine mkSkyStiff_Mat(gkl)
  use MATRIX_FORM
  real(8), intent(inout) :: gkl(:)
  integer :: ie,ip,i,nt,it,jp,j,jt,k,kk,iel,jel,m,ic
  real(8) :: s

  gkl(:) = 0.0d0

  do ie=1,MDL_elNum
    !	要素の応力-ひずみマトリクス作成
    call make_Dmatrix(ie)

    call mkStiff_elMat(ie)

    call assemSkyMat(ie, gkl(:), em(:,:))

  end do

end subroutine mkSkyStiff_Mat

!-------------------------------------------------------------------------------
! subroutine mkCSRStiff_Mat
!	要素剛性マトリクスのCSR形式マトリクスへの重ね合わせ
!-------------------------------------------------------------------------------
subroutine mkCSRStiff_Mat(ns, nf, gk_CSR)
  use MATRIX_FORM
  implicit none
  integer, intent(in) :: ns, nf
  real(8), intent(inout) :: gk_CSR(:)
  integer :: lo(24)
  integer :: nsf, row_head, row_tail, row_num
  integer :: ie,ip,i,nt,it,jp,j,jt,k,kk,iel,jel,m,ic
  real(8) :: s

  gk_CSR(:) = 0.0d0
  ! F_vec(:) = 0.0d0
  nsf = ns * nf

  do ie=1,MDL_elNum

  !	要素の応力-ひずみマトリクス作成
  call make_Dmatrix(ie)

  ! 要素剛性マトリクスの作成
  call mkStiff_elMat(ie)

  call assemCSRMat(ie, gk_CSR(:), em(:,:))

  end do

end subroutine mkCSRStiff_Mat

!===============================================================================
subroutine mkStiff_elMat(ie)
!	要素剛性マトリクスの作成
!===============================================================================
  integer,intent(in) :: ie

  integer, parameter :: point = 2
  integer :: i, j
  real(8) :: detJ
  real(8) :: Pt2_weight
  real(8),dimension(point**3) :: xi, eta, zeta
  real(8),dimension(8) :: dndxi, dndeta, dndzeta
  real(8),dimension(3,3) :: Jinv

  em = 0.0d0

  !	ガウス積分の積分点と重み
  call integral_2Pt(xi,eta,zeta,Pt2_weight)

  do i=1,point**3
    !	形状関数の微分
    call dshape_function(xi(i),eta(i),zeta(i),dndxi,dndeta,dndzeta)
    !	ヤコビ行列
    call make_Jmatrix(ie,dndxi,dndeta,dndzeta,detJ,Jinv)
    !	ひずみ-変位マトリクス
    call make_Bmatrix(dndxi,dndeta,dndzeta,Jinv)
    !	ガウス積分の計算
    call gaussStiff_el(Pt2_weight, detJ)
  end do

end subroutine mkStiff_elMat

!-------------------------------------------------------------------------------
! subroutine integral_2Pt
!	ガウス積分の積分点・重み係数
! 160618 @yamazaki 修正
!-------------------------------------------------------------------------------
subroutine integral_2Pt(xi,eta,zeta,weight)
  real(8), intent(out) :: weight
  real(8), dimension(2**3), intent(out) :: xi,eta,zeta

  integer :: i
  real(8) :: point1, w1, w2

  weight = 1.0d0

  point1 = sqrt(1.d0/3.d0)

  xi(1)  = -point1
  eta(1) = -point1
  zeta(1)= -point1
  xi(2)  =  point1
  eta(2) = -point1
  zeta(2)= -point1
  xi(3)  = -point1
  eta(3) =  point1
  zeta(3)= -point1
  xi(4)  =  point1
  eta(4) =  point1
  zeta(4)= -point1

  do i=1,4
    xi(4+i)  = xi(i)
    eta(4+i) = eta(i)
    zeta(4+i)= -zeta(i)
  end do

end subroutine integral_2Pt

!===============================================================================
subroutine make_Dmatrix(ie)
!	応力-ひずみマトリクスの作成
!===============================================================================
  use PHYSICAL_DATA
  integer,intent(in) :: ie

  integer :: i,j,material_ID
  real(8) :: young,poisson,temp

  material_ID = mte(ie)
  young   = young_list(material_ID)
  poisson = poisson_list(material_ID)

  temp = young / ((1.0d0+poisson)*(1.0d0-2.0d0*poisson))

  Dmatrix = 0.0d0
  do i=1,3
    Dmatrix(i,i)     = temp * (1.0d0 - poisson)
    Dmatrix(i+3,i+3) = temp * (1.0d0 - 2.0d0*poisson)/2.0d0
    do j=i+1,3
      Dmatrix(i,j) = temp * poisson
      Dmatrix(j,i) = temp * poisson
    end do
  end do

end subroutine

!===============================================================================
subroutine shape_function(xi,eta,zeta,shape_n)
!	形状関数
!===============================================================================
  real(8),intent(in) :: xi,eta,zeta
  real(8),dimension(8),intent(out) :: shape_n

  real(8) :: a1,a2,b1,b2,c1,c2


  a1 = 1.0d0 + xi
  b1 = 1.0d0 + eta
  c1 = 1.0d0 + zeta
  a2 = 1.0d0 - xi
  b2 = 1.0d0 - eta
  c2 = 1.0d0 - zeta

  !	形状関数Ni
  shape_n(1)= a2 * b2 * c2 * 1.0d0/8.0d0
  shape_n(2)= a1 * b2 * c2 * 1.0d0/8.0d0
  shape_n(3)= a1 * b1 * c2 * 1.0d0/8.0d0
  shape_n(4)= a2 * b1 * c2 * 1.0d0/8.0d0
  shape_n(5)= a2 * b2 * c1 * 1.0d0/8.0d0
  shape_n(6)= a1 * b2 * c1 * 1.0d0/8.0d0
  shape_n(7)= a1 * b1 * c1 * 1.0d0/8.0d0
  shape_n(8)= a2 * b1 * c1 * 1.0d0/8.0d0

end subroutine

!===============================================================================
subroutine dshape_function(xi,eta,zeta,dndxi,dndeta,dndzeta)
!	形状関数の微分(パラメトリック空間上の座標ξ,η,ζでそれぞれ微分)
!===============================================================================
  real(8),intent(in) :: xi,eta,zeta
  real(8),dimension(8),intent(out) :: dndxi,dndeta,dndzeta

  real(8) :: a1,a2,b1,b2,c1,c2


  a1 = 1.0d0 + xi
  b1 = 1.0d0 + eta
  c1 = 1.0d0 + zeta
  a2 = 1.0d0 - xi
  b2 = 1.0d0 - eta
  c2 = 1.0d0 - zeta

  !	∂Ni/∂ξ
  dndxi(1)= -b2 * c2 * 1.0d0/8.0d0
  dndxi(2)=  b2 * c2 * 1.0d0/8.0d0
  dndxi(3)=  b1 * c2 * 1.0d0/8.0d0
  dndxi(4)= -b1 * c2 * 1.0d0/8.0d0
  dndxi(5)= -b2 * c1 * 1.0d0/8.0d0
  dndxi(6)=  b2 * c1 * 1.0d0/8.0d0
  dndxi(7)=  b1 * c1 * 1.0d0/8.0d0
  dndxi(8)= -b1 * c1 * 1.0d0/8.0d0
  !	∂Ni/∂η
  dndeta(1)= -a2 * c2 * 1.0d0/8.0d0
  dndeta(2)= -a1 * c2 * 1.0d0/8.0d0
  dndeta(3)=  a1 * c2 * 1.0d0/8.0d0
  dndeta(4)=  a2 * c2 * 1.0d0/8.0d0
  dndeta(5)= -a2 * c1 * 1.0d0/8.0d0
  dndeta(6)= -a1 * c1 * 1.0d0/8.0d0
  dndeta(7)=  a1 * c1 * 1.0d0/8.0d0
  dndeta(8)=  a2 * c1 * 1.0d0/8.0d0
  !	∂Ni/∂ζ
  dndzeta(1)= -a2 * b2 * 1.0d0/8.0d0
  dndzeta(2)= -a1 * b2 * 1.0d0/8.0d0
  dndzeta(3)= -a1 * b1 * 1.0d0/8.0d0
  dndzeta(4)= -a2 * b1 * 1.0d0/8.0d0
  dndzeta(5)=  a2 * b2 * 1.0d0/8.0d0
  dndzeta(6)=  a1 * b2 * 1.0d0/8.0d0
  dndzeta(7)=  a1 * b1 * 1.0d0/8.0d0
  dndzeta(8)=  a2 * b1 * 1.0d0/8.0d0

end subroutine

!===============================================================================
subroutine make_Jmatrix(ie,dndxi,dndeta,dndzeta,detJ,Jinv)
!	ヤコビ行列(逆行列)の作成
!===============================================================================
  integer,intent(in) :: ie
  real(8),dimension(8),intent(in) :: dndxi,dndeta,dndzeta
  real(8),intent(out) :: detJ
  real(8),dimension(3,3),intent(out) :: Jinv

  integer :: i,ii
  real(8),dimension(3,3) :: Jm

  !	ヤコビ行列の作成
  Jm = 0.0d0
  do i=1,8
    ii = indv(ie,i)
    Jm(1,1) = Jm(1,1) + dndxi(i)  * MDL_nodePos(ii,1)
    Jm(2,1) = Jm(2,1) + dndeta(i) * MDL_nodePos(ii,1)
    Jm(3,1) = Jm(3,1) + dndzeta(i)* MDL_nodePos(ii,1)
    Jm(1,2) = Jm(1,2) + dndxi(i)  * MDL_nodePos(ii,2)
    Jm(2,2) = Jm(2,2) + dndeta(i) * MDL_nodePos(ii,2)
    Jm(3,2) = Jm(3,2) + dndzeta(i)* MDL_nodePos(ii,2)
    Jm(1,3) = Jm(1,3) + dndxi(i)  * MDL_nodePos(ii,3)
    Jm(2,3) = Jm(2,3) + dndeta(i) * MDL_nodePos(ii,3)
    Jm(3,3) = Jm(3,3) + dndzeta(i)* MDL_nodePos(ii,3)
  end do

  !	ヤコビ行列の逆行列の作成
  detJ = Jm(1,1)*(Jm(2,2)*Jm(3,3)-Jm(3,2)*Jm(2,3))&
    & + Jm(1,2)*(Jm(3,1)*Jm(2,3)-Jm(2,1)*Jm(3,3))&
    & + Jm(1,3)*(Jm(2,1)*Jm(3,2)-Jm(3,1)*Jm(2,2))

  Jinv(1,1) = (Jm(2,2)*Jm(3,3)-Jm(3,2)*Jm(2,3))/detJ
  Jinv(2,2) = (Jm(1,1)*Jm(3,3)-Jm(1,3)*Jm(3,1))/detJ
  Jinv(3,3) = (Jm(1,1)*Jm(2,2)-Jm(1,2)*Jm(2,1))/detJ

  Jinv(2,1) = (Jm(3,1)*Jm(2,3)-Jm(2,1)*Jm(3,3))/detJ
  Jinv(1,2) = (Jm(1,3)*Jm(3,2)-Jm(1,2)*Jm(3,3))/detJ

  Jinv(3,1) = (Jm(2,1)*Jm(3,2)-Jm(3,1)*Jm(2,2))/detJ
  Jinv(1,3) = (Jm(1,2)*Jm(2,3)-Jm(1,3)*Jm(2,2))/detJ

  Jinv(3,2) = (Jm(1,2)*Jm(3,1)-Jm(3,2)*Jm(1,1))/detJ
  Jinv(2,3) = (Jm(2,1)*Jm(1,3)-Jm(2,3)*Jm(1,1))/detJ

end subroutine make_Jmatrix

!===============================================================================
subroutine make_Bmatrix(dndxi,dndeta,dndzeta,Jinv)
!	ひずみ-変位マトリクスの作成
!===============================================================================
  real(8),dimension(8),intent(in) :: dndxi,dndeta,dndzeta
  real(8),dimension(3,3),intent(in) :: Jinv

  integer :: i,i1,i2,i3
  real(8) :: dndx,dndy,dndz


  do i=1,8
    i1 = 3*(i-1)+1
    i2 = 3*(i-1)+2
    i3 = 3*(i-1)+3
    dndx = Jinv(1,1)*dndxi(i)+Jinv(1,2)*dndeta(i)+Jinv(1,3)*dndzeta(i)
    dndy = Jinv(2,1)*dndxi(i)+Jinv(2,2)*dndeta(i)+Jinv(2,3)*dndzeta(i)
    dndz = Jinv(3,1)*dndxi(i)+Jinv(3,2)*dndeta(i)+Jinv(3,3)*dndzeta(i)

    Bmatrix(1,i1) = dndx
    Bmatrix(1,i2) = 0.0d0
    Bmatrix(1,i3) = 0.0d0
    Bmatrix(2,i1) = 0.0d0
    Bmatrix(2,i2) = dndy
    Bmatrix(2,i3) = 0.0d0
    Bmatrix(3,i1) = 0.0d0
    Bmatrix(3,i2) = 0.0d0
    Bmatrix(3,i3) = dndz

    Bmatrix(4,i1) = dndy
    Bmatrix(4,i2) = dndx
    Bmatrix(4,i3) = 0.0d0
    Bmatrix(6,i1) = 0.0d0
    Bmatrix(6,i2) = dndz
    Bmatrix(6,i3) = dndy
    Bmatrix(5,i1) = dndz
    Bmatrix(5,i2) = 0.0d0
    Bmatrix(5,i3) = dndx
  end do

end subroutine make_Bmatrix

!===============================================================================
subroutine gaussStiff_el(weight, detJ)
!	ガウス積分
!===============================================================================
  real(8), intent(in) :: weight, detJ
  real(8), dimension(24,6) :: btd

  !	[B]T[D]の計算
  btd = 0.d0
  call dgemm('T', 'N', 24, 6, 6, 1.d0, Bmatrix(1:6,1:24), 6, &
    & Dmatrix(1:6,1:6), 6, 0.d0, btd(1:24,1:6), 24)

  !	[B]T[D][B]の計算
  call dgemm('N', 'N', 24, 24, 6, weight * detJ, btd(1:24,1:6), 24, &
    & Bmatrix(1:6,1:24), 6, 1.d0, em(1:24,1:24), 24)

end subroutine gaussStiff_el

end module StiffMat_GenMOD
