!===============================================================================
! module GeoMat_GenMOD
! 線形座屈解析向けの幾何剛性の作成
!--------------------------
! gkt                : 幾何剛性マトリクス
! sigma0_matrix(9,9) : 初期応力マトリクス
! B0matrix(9,24)     : 名前忘れた。要素幾何剛性マトリクスに必要
! em0(24,24)         : 要素幾何剛性マトリクス
!===============================================================================

module GeoMat_GenMOD
  use MODEL_MOD
  implicit none
  real(8),dimension(9,9) :: sigma0_matrix
  real(8),dimension(9,24) :: B0matrix
  real(8),dimension(24,24) :: em0
  integer,parameter :: point = 2

contains

!====================================================================================================
! subroutine mkSkyGeo_Mat
!	要素剛性マトリクスの重ね合わせ
!====================================================================================================
subroutine mkSkyGeo_Mat(gkt)
  use MATRIX_FORM
  real(8), intent(inout) :: gkt(:)
  integer :: ie,ip,i,nt,it,jp,j,jt,kk,iel,jel

  gkt(:) = 0.0d0
  do ie=1,MDL_elNum

    ! 要素幾何剛性マトリクスの作成
    call mkGeo_elMat(ie)

    ! skylineマトリクスへの合成
    call assemSkyMat(ie, gkt, em0)

  end do
end subroutine mkSkyGeo_Mat

!-------------------------------------------------------------------------------
! subroutine mkCSRStiff_Mat
!	要素剛性マトリクスのCSR形式マトリクスへの重ね合わせ
!-------------------------------------------------------------------------------
subroutine mkCSRGeo_Mat(ns, nf, gk_CSR)
  use MATRIX_FORM
  implicit none
  integer, intent(in) :: ns, nf
  real(8), intent(inout) :: gk_CSR(:)
  integer :: lo(24)
  integer :: nsf, row_head, row_tail, row_num
	integer :: ie,ip,i,nt,it,jp,j,jt,k,kk,iel,jel,m,ic
	real(8) :: s

	gk_CSR(:) = 0.0d0
  nsf = ns * nf

	do ie=1,MDL_elNum

    ! 要素幾何剛性マトリクスの作成
    call mkGeo_elMat(ie)

    ! CSRマトリクスへの合成
    call assemCSRMat(ie, gk_CSR(:), em0(:,:))

  end do

end subroutine mkCSRGeo_Mat

!====================================================================================================
!	subroutine mkGeo_elMat(ie)
!	要素剛性マトリクスの作成
!====================================================================================================
subroutine mkGeo_elMat(ie)
integer,intent(in) :: ie

integer :: i,j
real(8) :: detJ, weight
real(8),dimension(point**3) :: xi,eta,zeta
real(8),dimension(8) :: shape_n,dndxi,dndeta,dndzeta
real(8),dimension(3,3) :: Jinv


em0 = 0.0d0

!	ガウス積分の積分点と重み
call integral_2Pt(xi,eta,zeta, weight)
!	応力-ひずみマトリクス
! call make_Sigma0_matrix(ie)

do i=1,point**3
  !	形状関数の微分
  call dshape_function(xi(i),eta(i),zeta(i),dndxi,dndeta,dndzeta)

  !	ヤコビ行列
  call make_Jmatrix(ie, dndxi,dndeta,dndzeta,detJ,Jinv)

  ! 積分点ごとに初期応力マトリクスを作成
  call Cal_Sig0_Matrix(ie, xi(i), eta(i), zeta(i), dndxi, dndeta, dndzeta, Jinv(:,:))

  !	ひずみ-変位マトリクス
  call make_B0matrix(dndxi, dndeta, dndzeta, Jinv)

  !	ガウス積分の計算
  call gauss(weight, detJ)

end do

end subroutine mkGeo_elMat

!===============================================================================
! subroutine Cal_Sig0_Matrix
! 積分点ごとの応力を算出
!===============================================================================
subroutine Cal_Sig0_Matrix(ie, xi, eta, zeta, dndxi, dndeta, dndzeta, Jinv)
  use StiffMat_GenMOD
  integer, intent(in) :: ie
  real(8), intent(in) :: xi, eta, zeta
  real(8), intent(in) :: dndxi(:), dndeta(:), dndzeta(:)
  real(8), intent(in) :: Jinv(3,3)
  ! real(8) :: detJ, Jinv(3,3)
  real(8) :: elem_disp(24)
  real(8) :: elem_ep(6)
  real(8) :: elem_0strss(6)

  integer :: info
  real(8) :: stressTensor(3,3), tmpTensor(3,3), tmpTensor2(3,3), priStress(3), work(10)

  integer :: i, j

  ! call dshape_function(xi, eta, zeta, dndxi(:), dndeta(:), dndzeta(:))
  !
  ! call make_Jmatrix(ie, dndxi(:), dndeta(:), dndzeta(:), detJ, Jinv(:,:))

  call make_Bmatrix(dndxi(:), dndeta(:), dndzeta(:), Jinv(:,:))

  do i = 1, 8
    do j = 1, 3
      elem_disp(3*(i-1)+j) = disp(3*(indv(ie,i)-1)+j)
    end do
  end do

  elem_ep(:) = matmul(Bmatrix(:,:),elem_disp(:))

  call make_Dmatrix(ie)

  elem_0strss(:) = 0.d0
  do i = 1, 6
    do j = 1, 6
      elem_0strss(i) = elem_0strss(i) + Dmatrix(i,j) * elem_ep(j)
    end do
  end do

  ! 主応力の変化を応力状態に与える
  ! stressTensor(1,1) = elem_0strss(1)
  ! stressTensor(2,2) = elem_0strss(2)
  ! stressTensor(3,3) = elem_0strss(3)
  ! stressTensor(2,1) = elem_0strss(4)
  ! stressTensor(3,1) = elem_0strss(5)
  ! stressTensor(3,2) = elem_0strss(6)
  !
  ! call dsyev('V', 'L', 3, stressTensor, 3, priStress(1:3), work, 10, info)
  ! if (info/=0) then
  !   print *, 'error occure at dsyev in Principal_Stress:'
  !   print *, 'error code : ', info
  ! end if
  !
  ! tmpTensor(:,:) = 0.d0
  ! do i = 1, 3
  !   tmpTensor(i,i) = priStress(i)
  ! end do
  ! call dgemm('N', 'N', 3, 3, 3, 1.d0, stressTensor, 3, tmpTensor, 3, 0.d0, tmpTensor2, 3)
  ! call dgemm('N', 'T', 3, 3, 3, 1.d0, tmpTensor2, 3, stressTensor, 3, 0.d0, tmpTensor, 3)
  !
  ! elem_0strss(1) = tmpTensor(1,1)
  ! elem_0strss(2) = tmpTensor(2,2)
  ! elem_0strss(3) = tmpTensor(3,3)
  ! elem_0strss(4) = tmpTensor(2,1)
  ! elem_0strss(5) = tmpTensor(3,1)
  ! elem_0strss(6) = tmpTensor(3,2)

  sigma0_matrix = 0.0d0
  do j = 1, 3
    i = (j-1) * 3 + 1
    sigma0_matrix(i,i) = elem_0strss(1)
    sigma0_matrix(i+1,i+1) = elem_0strss(2)
    sigma0_matrix(i+2,i+2) = elem_0strss(3)
  end do

  sigma0_matrix(1,2) = elem_0strss(4)
  sigma0_matrix(2,1) = elem_0strss(4)
  sigma0_matrix(4,5) = elem_0strss(4)
  sigma0_matrix(5,4) = elem_0strss(4)
  sigma0_matrix(7,8) = elem_0strss(4)
  sigma0_matrix(8,7) = elem_0strss(4)

  sigma0_matrix(1,3) = elem_0strss(5)
  sigma0_matrix(3,1) = elem_0strss(5)
  sigma0_matrix(4,6) = elem_0strss(5)
  sigma0_matrix(6,4) = elem_0strss(5)
  sigma0_matrix(7,9) = elem_0strss(5)
  sigma0_matrix(9,7) = elem_0strss(5)

  sigma0_matrix(2,3) = elem_0strss(6)
  sigma0_matrix(3,2) = elem_0strss(6)
  sigma0_matrix(5,6) = elem_0strss(6)
  sigma0_matrix(6,5) = elem_0strss(6)
  sigma0_matrix(8,9) = elem_0strss(6)
  sigma0_matrix(9,8) = elem_0strss(6)

end subroutine Cal_Sig0_Matrix

!-------------------------------------------------------------------------------
! subroutine integral_2Pt
!	ガウス積分の積分点・重み係数
! 160618 @yamazaki 修正
!-------------------------------------------------------------------------------
subroutine integral_2Pt(xi,eta,zeta,weight)
  real(8), intent(out) :: weight
  real(8), dimension(2**3), intent(out) :: xi,eta,zeta

  integer :: i
  real(8) :: point1, w1, w2

  weight = 1.0d0

  point1 = sqrt(1.d0/3.d0)

  xi(1)  = -point1
  eta(1) = -point1
  zeta(1)= -point1
  xi(2)  =  point1
  eta(2) = -point1
  zeta(2)= -point1
  xi(3)  = -point1
  eta(3) =  point1
  zeta(3)= -point1
  xi(4)  =  point1
  eta(4) =  point1
  zeta(4)= -point1

  do i=1,4
    xi(4+i)  = xi(i)
    eta(4+i) = eta(i)
    zeta(4+i)= -zeta(i)
  end do

end subroutine integral_2Pt

!====================================================================================================
subroutine make_Sigma0_matrix(ie)
!	応力-ひずみマトリクスの作成
! σx, σy, σz, τxy, τxz, τyz
!====================================================================================================
integer,intent(in) :: ie
integer :: i, j

sigma0_matrix = 0.0d0
do j = 1,3
  i = (j-1) * 3 + 1
  sigma0_matrix(i,i) = stress(ie, 1)
  sigma0_matrix(i+1,i+1) = stress(ie, 2)
  sigma0_matrix(i+2,i+2) = stress(ie, 3)
end do

sigma0_matrix(1,2) = stress(ie, 4)
sigma0_matrix(2,1) = stress(ie, 4)
sigma0_matrix(4,5) = stress(ie, 4)
sigma0_matrix(5,4) = stress(ie, 4)
sigma0_matrix(7,8) = stress(ie, 4)
sigma0_matrix(8,7) = stress(ie, 4)

sigma0_matrix(1,3) = stress(ie, 5)
sigma0_matrix(3,1) = stress(ie, 5)
sigma0_matrix(4,6) = stress(ie, 5)
sigma0_matrix(6,4) = stress(ie, 5)
sigma0_matrix(7,9) = stress(ie, 5)
sigma0_matrix(9,7) = stress(ie, 5)

sigma0_matrix(2,3) = stress(ie, 6)
sigma0_matrix(3,2) = stress(ie, 6)
sigma0_matrix(5,6) = stress(ie, 6)
sigma0_matrix(6,5) = stress(ie, 6)
sigma0_matrix(8,9) = stress(ie, 6)
sigma0_matrix(9,8) = stress(ie, 6)
end subroutine make_Sigma0_matrix

!====================================================================================================
subroutine dshape_function(xi,eta,zeta,dndxi,dndeta,dndzeta)
!	形状関数の微分(パラメトリック空間上の座標ξ,η,ζでそれぞれ微分)
!====================================================================================================
real(8),intent(in) :: xi,eta,zeta
real(8),dimension(8),intent(out) :: dndxi,dndeta,dndzeta

real(8) :: a1,a2,b1,b2,c1,c2


a1 = 1.0d0 + xi
b1 = 1.0d0 + eta
c1 = 1.0d0 + zeta
a2 = 1.0d0 - xi
b2 = 1.0d0 - eta
c2 = 1.0d0 - zeta

!	∂Ni/∂ξ
dndxi(1)= -b2 * c2 * 1.0d0/8.0d0
dndxi(2)=  b2 * c2 * 1.0d0/8.0d0
dndxi(3)=  b1 * c2 * 1.0d0/8.0d0
dndxi(4)= -b1 * c2 * 1.0d0/8.0d0
dndxi(5)= -b2 * c1 * 1.0d0/8.0d0
dndxi(6)=  b2 * c1 * 1.0d0/8.0d0
dndxi(7)=  b1 * c1 * 1.0d0/8.0d0
dndxi(8)= -b1 * c1 * 1.0d0/8.0d0
!	∂Ni/∂η
dndeta(1)= -a2 * c2 * 1.0d0/8.0d0
dndeta(2)= -a1 * c2 * 1.0d0/8.0d0
dndeta(3)=  a1 * c2 * 1.0d0/8.0d0
dndeta(4)=  a2 * c2 * 1.0d0/8.0d0
dndeta(5)= -a2 * c1 * 1.0d0/8.0d0
dndeta(6)= -a1 * c1 * 1.0d0/8.0d0
dndeta(7)=  a1 * c1 * 1.0d0/8.0d0
dndeta(8)=  a2 * c1 * 1.0d0/8.0d0
!	∂Ni/∂ζ
dndzeta(1)= -a2 * b2 * 1.0d0/8.0d0
dndzeta(2)= -a1 * b2 * 1.0d0/8.0d0
dndzeta(3)= -a1 * b1 * 1.0d0/8.0d0
dndzeta(4)= -a2 * b1 * 1.0d0/8.0d0
dndzeta(5)=  a2 * b2 * 1.0d0/8.0d0
dndzeta(6)=  a1 * b2 * 1.0d0/8.0d0
dndzeta(7)=  a1 * b1 * 1.0d0/8.0d0
dndzeta(8)=  a2 * b1 * 1.0d0/8.0d0

end subroutine dshape_function

!====================================================================================================
subroutine make_B0matrix(dndxi,dndeta,dndzeta,Jinv)
!	ひずみ-変位マトリクスの作成,６X24向けなので９X２４ようにしなければ
!====================================================================================================
real(8),dimension(8),intent(in) :: dndxi,dndeta,dndzeta
real(8),dimension(3,3),intent(in) :: Jinv

integer :: i,i1,i2,i3
real(8) :: dndx,dndy,dndz

B0matrix = 0.0d0
do i=1,8
  i1 = 3*(i-1)+1
  i2 = 3*(i-1)+2
  i3 = 3*(i-1)+3
  dndx = Jinv(1,1)*dndxi(i)+Jinv(1,2)*dndeta(i)+Jinv(1,3)*dndzeta(i)
  dndy = Jinv(2,1)*dndxi(i)+Jinv(2,2)*dndeta(i)+Jinv(2,3)*dndzeta(i)
  dndz = Jinv(3,1)*dndxi(i)+Jinv(3,2)*dndeta(i)+Jinv(3,3)*dndzeta(i)

  B0matrix(1,i1) = dndx
  B0matrix(2,i1) = dndy
  B0matrix(3,i1) = dndz
  B0matrix(4,i2) = dndx
  B0matrix(5,i2) = dndy
  B0matrix(6,i2) = dndz
  B0matrix(7,i3) = dndx
  B0matrix(8,i3) = dndy
  B0matrix(9,i3) = dndz

end do

end subroutine make_B0matrix

!====================================================================================================
subroutine make_Jmatrix(ie,dndxi,dndeta,dndzeta,detJ,Jinv)
!	ヤコビ行列(逆行列)の作成
!====================================================================================================
integer,intent(in) :: ie
real(8),dimension(8),intent(in) :: dndxi,dndeta,dndzeta
real(8),intent(out) :: detJ
real(8),dimension(3,3),intent(out) :: Jinv

integer :: i,ii
real(8),dimension(3,3) :: Jm

!	ヤコビ行列の作成
Jm = 0.0d0
do i=1,8
  ii = indv(ie,i)
  Jm(1,1) = Jm(1,1) + dndxi(i)  * MDL_nodePos(ii,1)
  Jm(2,1) = Jm(2,1) + dndeta(i) * MDL_nodePos(ii,1)
  Jm(3,1) = Jm(3,1) + dndzeta(i)* MDL_nodePos(ii,1)
  Jm(1,2) = Jm(1,2) + dndxi(i)  * MDL_nodePos(ii,2)
  Jm(2,2) = Jm(2,2) + dndeta(i) * MDL_nodePos(ii,2)
  Jm(3,2) = Jm(3,2) + dndzeta(i)* MDL_nodePos(ii,2)
  Jm(1,3) = Jm(1,3) + dndxi(i)  * MDL_nodePos(ii,3)
  Jm(2,3) = Jm(2,3) + dndeta(i) * MDL_nodePos(ii,3)
  Jm(3,3) = Jm(3,3) + dndzeta(i)* MDL_nodePos(ii,3)
end do

!	ヤコビ行列の逆行列の作成
detJ = Jm(1,1)*(Jm(2,2)*Jm(3,3)-Jm(3,2)*Jm(2,3))&
   + Jm(1,2)*(Jm(3,1)*Jm(2,3)-Jm(2,1)*Jm(3,3))&
   + Jm(1,3)*(Jm(2,1)*Jm(3,2)-Jm(3,1)*Jm(2,2))

Jinv(1,1) = (Jm(2,2)*Jm(3,3)-Jm(3,2)*Jm(2,3))/detJ
Jinv(2,2) = (Jm(1,1)*Jm(3,3)-Jm(1,3)*Jm(3,1))/detJ
Jinv(3,3) = (Jm(1,1)*Jm(2,2)-Jm(1,2)*Jm(2,1))/detJ

Jinv(2,1) = (Jm(3,1)*Jm(2,3)-Jm(2,1)*Jm(3,3))/detJ
Jinv(1,2) = (Jm(1,3)*Jm(3,2)-Jm(1,2)*Jm(3,3))/detJ

Jinv(3,1) = (Jm(2,1)*Jm(3,2)-Jm(3,1)*Jm(2,2))/detJ
Jinv(1,3) = (Jm(1,2)*Jm(2,3)-Jm(1,3)*Jm(2,2))/detJ

Jinv(3,2) = (Jm(1,2)*Jm(3,1)-Jm(3,2)*Jm(1,1))/detJ
Jinv(2,3) = (Jm(2,1)*Jm(1,3)-Jm(2,3)*Jm(1,1))/detJ


end subroutine

!====================================================================================================
! subroutine gauss
!	ガウス積分
!====================================================================================================
subroutine gauss(w,detJ)
  real(8),intent(in) :: w,detJ
  real(8), dimension(24,9) :: btd

  !	[B]T[D]の計算
  btd = 0.0d0
  call dgemm('T','N', 24, 9, 9, 1.d0, B0matrix(1:9,1:24), 9, &
    & sigma0_matrix(1:9,1:9), 9, 0.d0, btd(1:24,1:9), 24)

  !	[B]T[D][B]の計算
  call dgemm('N', 'N', 24, 24, 9, w * detJ, btd(1:24,1:9), 24, &
    & B0matrix(1:9,1:24), 9, 1.d0, em0(1:24,1:24), 24)

end subroutine gauss

end module GeoMat_GenMOD
