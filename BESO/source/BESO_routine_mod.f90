module BESO_ROUTINE
  use BESO_DATA
  implicit none
  contains

!-------------------------------------------------------------------------------
! subroutine BuckSense_Mix
! 座屈感度係数の合成
!-------------------------------------------------------------------------------
subroutine BuckSense_Mix
  real(8) :: weight_bottom

  integer :: i, j
  do i = 1, totalAnalyCase
    weight_bottom = 0.d0
    do  j = 1, BsenJoinNum
      weight_bottom = weight_bottom + 1.d0 / (buckLoad(j,i)**Bsen_mixPenalty)
    end do

    buckSense(:,i) = 0.d0
    do j = 1, BsenJoinNum
      buckSense(:,i) = buckSense(:,i) + buckSense_list(:,j,i) * buckLoad(1,i) / &
        & (buckLoad(j,i)**(Bsen_mixPenalty) * weight_bottom)
    end do
  end do

end subroutine BuckSense_Mix

!-------------------------------------------------------------------------------
! subroutine BuckSense_Equalize
! 感度係数の分布の2次モーメントを統一する。
!-------------------------------------------------------------------------------
subroutine BuckSense_Equalize
  real(8), allocatable :: list_pack(:)
  integer :: i, j
  allocate(list_pack(count(continuous_model(:)==1)))
  do i = 1, totalAnalyCase
    do j = 1, BsenJoinNum
      list_pack(:) = pack(buckSense_list(:,j,i), continuous_model(:)==1)
      call Distribute_Remapper(list_pack(:))
      buckSense_list(:,j,i) = unpack(list_pack(:), continuous_model(:)==1, field=buckSense_list(:,j,i))
    end do
  end do
contains
  subroutine Distribute_Remapper(list)
    real(8), intent(inout) :: list(:)

    integer :: list_size, target_num
    real(8) :: min_sense, max_sense, delta_sense
    real(8), allocatable :: list_copy(:)

    integer :: i

    list_size = size(list)
    min_sense = minval(list)
    max_sense = maxval(list)
    delta_sense = 1.d0 / list_size
    ! delta_sense = (max_sense - min_sense) / list_size

    allocate(list_copy(list_size)); list_copy(:) = 0
    do i = 1, list_size
      target_num = count(list(:)<list(i))
      list_copy(i) = target_num * delta_sense
      ! list_copy(i) = min_sense + target_num * delta_sense
    end do

    ! 配列を転写して終了
    list(:) = list_copy

  end subroutine Distribute_Remapper
end subroutine BuckSense_Equalize

!-------------------------------------------------------------------------------
! subroutine BuckSense_Brending
! 感度係数の上限下限を基準化する。
!-------------------------------------------------------------------------------
subroutine BuckSense_Brending
  real(8) :: base_width, weight_bottom
  real(8) :: buckSense_temp(El,BsenJoinNum)
  integer :: i, j
  do i = 1, totalAnalyCase
    base_width = maxval(buckSense_list(:,1,i)) - minval(buckSense_list(:,1,i))
    buckSense_temp(:,1) = buckSense_list(:,1,i)
    do j = 2, BsenJoinNum
      buckSense_temp(:,j) = base_width/(maxval(buckSense_list(:,j,i)) - minval(buckSense_list(:,j,i))) &
        & * buckSense_list(:,j,1)
    end do

    weight_bottom = 0.d0
    do  j = 1, BsenJoinNum
      weight_bottom = weight_bottom + 1.d0 / (buckLoad(j,i)**Bsen_mixPenalty)
    end do

    buckSense(:,i) = 0.d0
    do j = 1, BsenJoinNum
      buckSense(:,i) = buckSense(:,i) + buckSense_temp(:,j) / &
        & (buckLoad(j,i)**(Bsen_mixPenalty) * weight_bottom)
    end do
  end do
end subroutine BuckSense_Brending

!-------------------------------------------------------------------------------
! subroutine ChangeRateMulti_Sense
! 座屈荷重係数の変化率を掛け合わせる
!-------------------------------------------------------------------------------
subroutine ChangeRateMulti_Sense
  integer :: i
  do i = 1, El
    Sensitivity(i) = dispSense(i,1) * buckLoad(1,1) / (buckLoad(1,1) - buckSense_list(i,1,1))
  end do
end subroutine ChangeRateMulti_Sense

!-------------------------------------------------------------------------------
! subroutine SenVal_Comp
! 敏感数の合成と全体領域への移動
! 敏感数の対応番号(1:Von Mises,  2:変位敏感数, 3:座屈敏感数, 4:昼光敏感数)
!-------------------------------------------------------------------------------
subroutine SenVal_Compose
  ! use MODEL_MOD
  implicit none

  real(8) :: maxVon, maxDisp, maxBuck,maxDaylight
  integer :: i, j

  Sensitivity(:) = 0.d0
  do i = 1, totalAnalyCase
    maxVon = maxval(VonMises(:,i)) + 1.d-13
    maxDisp = maxval(dispSense(:,i)) + 1.d-13
    maxBuck = maxval(buckSense(:,i)) + 1.d-13
    maxDaylight= maxval(Daylight_fac_Sense(:,i)) + 1.d-13
    ! Mises応力の足し合わせ
    do j = 1, El
      Sensitivity(j) = Sensitivity(j) + coeff_von * VonMises(j,i) / maxVon
    end do

    ! 変位敏感数の足し合わせ
    do j = 1, El
      Sensitivity(j) = Sensitivity(j) + coeff_disp * dispSense(j,i) / maxDisp
    end do

    ! 座屈敏感数の足し合わせ
    do j = 1, El
      Sensitivity(j) = Sensitivity(j) + coeff_buck * buckSense(j,i) / maxBuck
    end do
    !昼光敏感数の足し合わせ
    do j = 1, El
      Sensitivity(j) = Sensitivity(j) + coeff_daylight * Daylight_fac_Sense(j,i) / maxDaylight
    end do
  end do
   
  
end subroutine SenVal_Compose

!-------------------------------------------------------------------------------
! subroutine SymCopy
! 感度係数の対称性を反映
!-------------------------------------------------------------------------------
subroutine SymCopy
  integer :: symNode_Num, symNum
  real(8) :: tmp_val

  integer :: i, j

  symNode_Num = size(sym_el(:,:), dim=1)
  symNum = size(sym_el(:,:), dim=2)

  do i = 1, symNode_Num
    ! 対称な要素の平均感度係数を算出
    tmp_val = 0.d0
    do j = 1, symNum
      tmp_val = tmp_val + Sensitivity(sym_el(i,j))
    end do
    tmp_val = tmp_val / symNum

    ! 平均を代入
    do j = 1, symNum
      Sensitivity(sym_el(i,j)) = tmp_val
    end do
  end do

end subroutine SymCopy

!-------------------------------------------------------------------------------
! subroutine Remove_Isolate
! 要素の連続性をマッピング
!-------------------------------------------------------------------------------
subroutine Remove_Isolate
  integer :: stack_num(2), elm_stack(El,2)

  integer :: neigh_type = 1 ! 1:ノイマン境界, 2:ディリクレ境界

  integer :: target_elm, target_ix, target_iy, target_iz
  integer :: srch_elm, srch_ix, srch_iy, srch_iz
  integer :: count_zero
  integer, allocatable :: continuous_map(:,:)

  integer :: i

  allocate(continuous_map(El,2))
  continuous_map(:,1) = -Be(:)
  continuous_map(:,2) = continuous_map(:,1)
  stack_num(:) = 0
  elm_stack(:,:) = 0

  ! 支持要素でかつ、位相内部(Be=1)に1を入力
  do i = 1, size(supportEl_list)
    if (Be(supportEl_list(i))==1) then
      continuous_map(supportEl_list(i),1) = 1
      stack_num(1) = stack_num(1) + 1
      elm_stack(stack_num(1),1) = supportEl_list(i)
    end if
  end do

  ! 荷重要素でかつ、位相内部(Be=1)に1を入力
  do i = 1, size(loadEl_list)
    if (Be(loadEl_list(i))==1) then
      continuous_map(loadEl_list(i),2) = 1
      stack_num(2) = stack_num(2) + 1
      elm_stack(stack_num(2),2) = loadEl_list(i)
    end if
  end do

  ! スタックを消費し切るまで回る
  do i = 1, 2
    srch_roop: do
      target_elm = elm_stack(stack_num(i),i)
      stack_num(i) = stack_num(i) - 1

      target_iz = target_elm / (nx*ny)
      if (mod(target_elm, (nx*ny)) > 0) target_iz = target_iz + 1

      target_iy = mod(target_elm, (nx*ny)) / nx
      if (mod(mod(target_elm, (nx*ny)), nx) > 0) target_iy = target_iy + 1

      target_ix = mod(target_elm, nx)
      if (target_ix==0) target_ix = nx

      do srch_ix = target_ix - 1, target_ix + 1
        ! インデックスが領域外部
        if (srch_ix <= 0) cycle
        if (srch_ix > nx) cycle

        do srch_iy = target_iy - 1, target_iy + 1
          ! インデックスが領域外部
          if (srch_iy <= 0) cycle
          if (srch_iy > ny) cycle

          do srch_iz = target_iz - 1, target_iz + 1
            ! インデックスが領域外部
            if (srch_iz <= 0) cycle
            if (srch_iz > nz) cycle

            srch_elm = Total_ID(srch_ix, srch_iy, srch_iz)
            ! 探索先要素が位相外部
            if (continuous_map(srch_elm, i)==0) cycle

            ! 探索先要素がすでに探索済み
            if (continuous_map(srch_elm, i)==1) cycle

            ! 境界条件によって選別
            count_zero = count((/target_ix==srch_ix, target_iy==srch_iy, target_iz==srch_iz/))
            ! 真ん中は自分自身なので回避
            if (count_zero >= 3) cycle

            ! ノイマン境界タイプでは、面が接しないときは回避
            if (neigh_type == 1) then
              if (count_zero < 2) cycle
            end if

            ! ここに到達すると
            ! target要素とsrch要素が隣接
            ! スタックに新しく見つかった要素を追加
            stack_num(i) = stack_num(i) + 1
            elm_stack(stack_num(i),i) = srch_elm

            ! 連続性を更新
            continuous_map(srch_elm, i) = 1
          end do
        end do
      end do

      ! ディリクレ境界チェック

      ! 直交インデックスが領域外を示している。→next
      ! 位相外部(continuous_map=0)→next
      ! すでに連続性を確認済み(continuous_map=1)→next
      ! 連続性を発見(continuous_map=-1)
      !   →スタックに新しく見つかった要素を追加・連続性を更新→next

      if (stack_num(i) <= 0) exit srch_roop

    end do srch_roop

  end do

  do i = 1, El
    if (all(continuous_map(i,:)==1)) then
      continuous_model(i) = 1
    else
      continuous_model(i) = 0
    end if
  end do

  ! print *, 'continue:', count(continuous_map(:,1)==1), count(continuous_map(:,2)==1)
  ! print *, 'Be :', count(Be(:)==1)

end subroutine Remove_Isolate

!-------------------------------------------------------------------------------
! function Total_ID
! 直交座標インデックスを通しのインデックスに変換
!-------------------------------------------------------------------------------
integer function Total_ID(ix, iy, iz)
  integer, intent(in) :: ix, iy, iz
  Total_ID = (iz-1) * (nx*ny) + (iy-1) * nx + ix
end function Total_ID

!-------------------------------------------------------------------------------
! subroutine filter_scheme
! 感度フィルタリング
! 要素値のフィルタリング
!-------------------------------------------------------------------------------
subroutine filter_scheme(origin_array, filter_array, filter_Mat, filter_width_x, filter_width_y, filter_width_z)
  use BESO_SUPER
  real(8), intent(in) :: origin_array(El)
  real(8), intent(in) :: filter_Mat(:,:,:)
  real(8), intent(out) :: filter_array(El)
  integer, intent(in) :: filter_width_x, filter_width_y, filter_width_z

  real(8) :: dim3_Mat(nx,ny,nz)
  integer, allocatable :: filter_effMap(:,:,:)
  real(8), allocatable :: filter_target(:,:,:)
  integer :: x_range(2), y_range(2), z_range(2)

  integer :: flat_index
  integer :: ix, iy, iz

  ! filter_width_x = filter_size_x / 2
  ! filter_width_y = filter_size_y / 2
  ! filter_width_z = filter_size_z / 2

  allocate(filter_effMap(-filter_width_x:filter_width_x,-filter_width_y:filter_width_y,-filter_width_z:filter_width_z))
  allocate(filter_target(-filter_width_x:filter_width_x,-filter_width_y:filter_width_y,-filter_width_z:filter_width_z))
  dim3_Mat(:,:,:) = reshape(origin_array(:), shape=(/nx,ny,nz/))

  filter_array(:) = 0.d0
  do iz = 1, nz
    if (iz - filter_width_z < 1) then
      z_range(1) = 1 - iz
    else
      z_range(1) = -filter_width_z
    end if
    if (iz + filter_width_z > nz) then
      z_range(2) = nz - iz
    else
      z_range(2) = filter_width_z
    end if
    do iy = 1, ny
      if (iy - filter_width_y < 1) then
        y_range(1) = 1 - iy
      else
        y_range(1) = -filter_width_y
      end if
      if (iy + filter_width_y > ny) then
        y_range(2) = ny - iy
      else
        y_range(2) = filter_width_y
      end if
      do ix = 1, nx
        if (ix - filter_width_x < 1) then
          x_range(1) = 1 - ix
        else
          x_range(1) = -filter_width_x
        end if
        if (ix + filter_width_x > nx) then
          x_range(2) = nx - ix
        else
          x_range(2) = filter_width_x
        end if

        filter_effMap(:,:,:) = 0
        filter_effMap(x_range(1):x_range(2), y_range(1):y_range(2), &
        & z_range(1):z_range(2)) = 1

        filter_target(:,:,:) = 0.d0
        filter_target(x_range(1):x_range(2), y_range(1):y_range(2), &
        & z_range(1):z_range(2)) = &
        & dim3_Mat(ix + x_range(1):ix + x_range(2), iy + y_range(1):iy + y_range(2), iz + z_range(1):iz + z_range(2))

        call filter_hokan(x_range, y_range, z_range, filter_width_x, filter_width_y, filter_width_z, filter_target(:,:,:))

        ! フィルター適用要素の通しのインデックス
        flat_index = ((iz-1)*ny + (iy-1))*nx + ix
        filter_array(flat_index) = sum(filter_target(:,:,:) * filter_Mat(:,:,:))
        filter_array(flat_index) = filter_array(flat_index) / sum(filter_Mat(:,:,:))
        ! filter_array(flat_index) = filter_array(flat_index) / sum(filter_Mat(:,:,:), filter_effMap(:,:,:)==1)
      end do
    end do
  end do

end subroutine filter_scheme

!-------------------------------------------------------------------------------
! subroutine filter_hokan
!-------------------------------------------------------------------------------
subroutine filter_hokan(x_range, y_range, z_range, filter_width_x, filter_width_y, filter_width_z, filter_target)
  integer, intent(in) :: x_range(2), y_range(2), z_range(2)
  integer, intent(in) :: filter_width_x, filter_width_y, filter_width_z
  real(8), intent(inout) :: filter_target(-filter_width_x:filter_width_x,-filter_width_y:filter_width_y,-filter_width_z:filter_width_z)

  integer :: i

  ! 線の延長線上
  do i = -filter_width_x, x_range(1) - 1
    filter_target(i,y_range(1):y_range(2),z_range(1):z_range(2)) = filter_target(x_range(1),y_range(1):y_range(2),z_range(1):z_range(2))
  end do

  do i = x_range(2) + 1, filter_width_x
    filter_target(i,y_range(1):y_range(2),z_range(1):z_range(2)) = filter_target(x_range(2),y_range(1):y_range(2),z_range(1):z_range(2))
  end do

  do i = -filter_width_y, y_range(1) - 1
    filter_target(x_range(1):x_range(2),i,z_range(1):z_range(2)) = filter_target(x_range(1):x_range(2),y_range(1),z_range(1):z_range(2))
  end do

  do i = y_range(2) + 1, filter_width_y
    filter_target(x_range(1):x_range(2),i,z_range(1):z_range(2)) = filter_target(x_range(1):x_range(2),y_range(2),z_range(1):z_range(2))
  end do

  do i = -filter_width_z, z_range(1) - 1
    filter_target(x_range(1):x_range(2),y_range(1):y_range(2),i) = filter_target(x_range(1):x_range(2),y_range(1):y_range(2),z_range(1))
  end do

  do i = z_range(2) + 1, filter_width_z
    filter_target(x_range(1):x_range(2),y_range(1):y_range(2),i) = filter_target(x_range(1):x_range(2),y_range(1):y_range(2),z_range(2))
  end do

  ! 面の角
  do i = x_range(1), x_range(2)
    filter_target(i,:y_range(1)-1,:z_range(1)-1) = filter_target(i,y_range(1),z_range(1))
    filter_target(i,y_range(2)+1:,z_range(2)+1:) = filter_target(i,y_range(2),z_range(2))
    filter_target(i,:y_range(1)-1,z_range(2)+1:) = filter_target(i,y_range(1),z_range(2))
    filter_target(i,y_range(2)+1:,:z_range(1)-1) = filter_target(i,y_range(2),z_range(1))
  end do

  do i = y_range(1), y_range(2)
    filter_target(:x_range(1)-1,i,:z_range(1)-1) = filter_target(x_range(1),i,z_range(1))
    filter_target(x_range(2)+1:,i,z_range(2)+1:) = filter_target(x_range(2),i,z_range(2))
    filter_target(:x_range(1)-1,i,z_range(2)+1:) = filter_target(x_range(1),i,z_range(2))
    filter_target(x_range(2)+1:,i,:z_range(1)-1) = filter_target(x_range(2),i,z_range(1))
  end do

  do i = z_range(1), z_range(2)
    filter_target(:x_range(1)-1,:y_range(1)-1,i) = filter_target(x_range(1),y_range(1),i)
    filter_target(x_range(2)+1:,y_range(2)+1:,i) = filter_target(x_range(2),y_range(2),i)
    filter_target(x_range(2)+1:,:y_range(1)-1,i) = filter_target(x_range(2),y_range(1),i)
    filter_target(:x_range(1)-1,y_range(2)+1:,i) = filter_target(x_range(1),y_range(2),i)
  end do

  ! 角の角
  filter_target(-filter_width_x:x_range(1)-1,-filter_width_y:y_range(1)-1,-filter_width_z:z_range(1)-1) = filter_target(x_range(1),y_range(1),z_range(1))
  filter_target( x_range(2)+1:filter_width_x,-filter_width_y:y_range(1)-1,-filter_width_z:z_range(1)-1) = filter_target(x_range(2),y_range(1),z_range(1))
  filter_target(-filter_width_x:x_range(1)-1, y_range(2)+1:filter_width_y,-filter_width_z:z_range(1)-1) = filter_target(x_range(1),y_range(2),z_range(1))
  filter_target( x_range(2)+1:filter_width_x, y_range(2)+1:filter_width_y,-filter_width_z:z_range(1)-1) = filter_target(x_range(2),y_range(2),z_range(1))
  filter_target(-filter_width_x:x_range(1)-1,-filter_width_y:y_range(1)-1, z_range(2)+1:filter_width_z) = filter_target(x_range(1),y_range(1),z_range(2))
  filter_target( x_range(2)+1:filter_width_x,-filter_width_y:y_range(1)-1, z_range(2)+1:filter_width_z) = filter_target(x_range(2),y_range(1),z_range(2))
  filter_target(-filter_width_x:x_range(1)-1, y_range(2)+1:filter_width_y, z_range(2)+1:filter_width_z) = filter_target(x_range(1),y_range(2),z_range(2))
  filter_target( x_range(2)+1:filter_width_x, y_range(2)+1:filter_width_y, z_range(2)+1:filter_width_z) = filter_target(x_range(2),y_range(2),z_range(2))

end subroutine filter_hokan

!-------------------------------------------------------------------------------
! subroutine filter_scheme_gauss
! 感度フィルタリング
! 要素値のフィルタリング
!-------------------------------------------------------------------------------
subroutine filter_scheme_gauss(origin_array, filter_array)
  use BESO_SUPER
  real(8), intent(in) :: origin_array(El)
  real(8), intent(out) :: filter_array(El)

  integer :: ix_head, ix_tail, iy_head, iy_tail, iz_head, iz_tail
  integer :: ix_range, iy_range, iz_range
  integer :: self_index, target_index, count_list_ind
  real(8) :: target_dist

  real(8), allocatable :: chk_list(:)
  integer :: i, ix, iy, iz, jx, jy, jz

  ix_range = int(r_min1 / hx)
  iy_range = int(r_min1 / hy)
  iz_range = int(r_min1 / hz)

  allocate(chk_list((ix_range*2+1)*(iy_range*2+1)*(iz_range*2+1)))

  filter_array(:) = 0.d0
  do iz = 1, nz
    iz_head = max(iz - iz_range, 1)
    iz_tail = min(nz, iz + iz_range)
    do iy = 1, ny
      iy_head = max(iy - iy_range, 1)
      iy_tail = min(ny, iy + iy_range)
      do ix = 1, nx
        ix_head = max(ix - ix_range, 1)
        ix_tail = min(nx, ix + ix_range)
        ! フィルター適用要素の通しのインデックス
        self_index = ((iz-1)*ny + (iy-1))*nx + ix

        !------調査範囲内を調査------
        count_list_ind = 0
        chk_list(:) = 0.d0
        do jz = iz_head, iz_tail
          do jy = iy_head, iy_tail
            do jx = ix_head, ix_tail
              ! 調査点の通しのインデックス
              target_index = ((jz-1)*ny + (jy-1))*nx + jx
              ! 調査点との距離
              target_dist = sqrt(sum((EL_g(target_index,:) - EL_g(self_index,:))**2))

              if (target_dist > r_min1) cycle
              ! 重み係数の保存
              count_list_ind = count_list_ind + 1

              ! sigma = r_min1
              chk_list(count_list_ind) = exp(target_dist**2.d0/(-2.d0*(r_min1*2.d0))) &
                & / ((2.d0 * pi)**1.5d0 * (r_min1*2.d0)**1.5d0)

              ! 重みを掛けて足し合わせ
              filter_array(self_index) = filter_array(self_index) + chk_list(count_list_ind) * origin_array(target_index)
            end do
          end do
        end do
        ! 重み係数の総和で割る
        filter_array(self_index) = filter_array(self_index) / sum(chk_list(1:count_list_ind))
        !------調査範囲内を調査------

      end do
    end do
  end do

end subroutine filter_scheme_gauss

!-------------------------------------------------------------------------------
! subroutine Generate_NextModel
! 次ステップのモデルを作成
!-------------------------------------------------------------------------------
subroutine Generate_NextModel
  use BESO_DATA
  use BESO_SUPER
  use STD_CAL
  use PHYSICAL_DATA
  use model_routines
  implicit none

  real(8) :: sense_ope(ElNum_ope)
  real(8) :: stress_ope(Elnum_ope)
  real(8) :: tmp_pack(ElNum_ope)

  integer, allocatable :: Be_local(:)
  integer :: next_volume
  integer, save :: count_convergence = 0
  real(8) :: current_rate
  real(8) :: err = 1.d-13

  real(8) :: find_rate

  integer :: i, j

  sense_ope(1:ElNum_ope) = pack(pack(El_sen(:), sym_el_map(:)==1), Cre_Map_local(:)==1)
  stress_ope(1:ElNum_ope)= pack(pack(vonMises(1,:), sym_el_map(:)==1), Cre_Map_local(:)==1)
  
  allocate(Be_local(sym_elmNum))
  Be_local(1:sym_elmNum) = pack(Be(:), sym_el_map(:)==1)

  ! 次ステップの要素数の決定
  current_rate = dble(BE_ElmNum_ope)/dble(ElNum_ope)
  if (abs(target_rate - current_rate) < e_rate) then
    next_volume = ElNum_ope * target_rate
  else
    next_volume = ElNum_ope * (sign(1.d0,target_rate - current_rate) * e_rate + current_rate)
  end if

  ! topomove_num
  if (topo_rate >= 0.d0) then
    call Find_new_sense(sense_hist_ope(:), sense_ope(:), ElNum_ope, next_volume, int(topo_rate*ElNum_ope), find_rate)
  else
    sense_hist_ope(:) = sense_ope(:)
  end if

  ! 基準値の決定
  tmp_pack(:) = dqsort(sense_hist_ope(:))
  Vp0 = tmp_pack(ElNum_ope - next_volume)

  do i = 1, ElNum_ope
    if (sense_hist_ope(i) >= Vp0) then
    ! if (sense_hist_ope(i) >= Vp0 - err) then
      Be_ope(i) = 1
    else
      Be_ope(i) = 0
    end if
  end do

  
  BE_ElmNum_ope = count(Be_ope(:)==1)
  Be_local(:) = unpack(Be_ope(:), Cre_Map_local(:)==1, Be_local(:))

  ! 対称部分にミラー
  do i = 1, sym_elmNum
    do j = 1, sym_num
      Be(sym_el(i,j)) = Be_local(i)
    end do
  end do

  ! 収束判定
  if (kstep > 1) then
    ! 位相変化量が移動幅の内部
    if (e_rate/2.d0*BE_ElmNum_ope <= count(Be_pre(:) /= Be(:))) then
      if ( target_El*(1.d0 - e_rate/2.d0) <= BE_ElmNum_ope &
        & .and. BE_ElmNum_ope <= target_El*(1.d0 + e_rate/2.d0) ) then
        count_convergence = count_convergence + 1
        if (count_convergence >= convergence_num) then
          stop "count_convergence is reached to convergence_num"
        end if
      else
        count_convergence = 0
      end if
    else
      count_convergence = 0
    end if
  end if

  Be_pre(:) = Be(:)
  El1 = count(Be(:)==1)

end subroutine Generate_NextModel

!-------------------------------------------------------------------------------
! subroutine Find_new_sense
! トポロジー変化量を満たす感度場を算出
!-------------------------------------------------------------------------------
subroutine Find_new_sense(sense_pre, sense_new, variable_num, inside_num, topomove_num, div_rate)
  use STD_CAL
  real(8), intent(inout) :: sense_pre(:)
  real(8), intent(in) :: sense_new(:)
  integer, intent(in) :: variable_num
  integer, intent(in) :: inside_num
  integer, intent(in) :: topomove_num
  real(8), intent(out) :: div_rate

  integer :: roop_max = 100
  real(8) :: div_head, div_tail
  real(8) :: bound_val ! しきい値
  integer :: move_cell
  integer :: topo_map_pre(variable_num), topo_map(variable_num)
  real(8) :: sense_mid(variable_num)
  real(8) :: tmp_listsort(variable_num)

  integer :: roop_count
  integer :: i

  div_rate = 1.d0

  ! 初期マップの作成
  tmp_listsort(:) = dqsort(sense_pre(:))
  bound_val = tmp_listsort(variable_num - inside_num + 1)
  do i = 1, variable_num
    if(sense_pre(i) >= bound_val) then
      topo_map_pre(i) = 1
    else
      topo_map_pre(i) = 0
    end if
  end do

  ! 次ステップの感度による位相変化量のチェック
  tmp_listsort(:) = dqsort(sense_new(:))
  bound_val = tmp_listsort(variable_num - inside_num + 1)
  do i = 1, variable_num
    if(sense_new(i) >= bound_val) then
      topo_map(i) = 1
    else
      topo_map(i) = 0
    end if
  end do

  move_cell = count(topo_map_pre(:)/=topo_map(:))

  print *, 'movecell origin :', move_cell

  ! 位相変化量が十分小さい
  if (move_cell <= topomove_num) then
    sense_pre(:) = sense_new(:)
    print *, 'normal'
    return
  end if

  ! 位相変化量に基づく次ステップマップの決定
  div_rate = 0.5d0
  div_head = 1.d0
  div_tail = 0.d0
  roop_count = 0
  do
    sense_mid(:) = sense_pre(:)*(1.d0 - div_rate) + sense_new(:)*div_rate

    tmp_listsort(:) = dqsort(sense_mid(:))
    bound_val = tmp_listsort(variable_num - inside_num + 1)
    do i = 1, variable_num
      if(sense_mid(i) >= bound_val) then
        topo_map(i) = 1
      else
        topo_map(i) = 0
      end if
    end do

    move_cell = count(topo_map_pre(:)/=topo_map(:))

    ! 2分探索の更新
    if (move_cell < topomove_num) then
      div_tail = div_rate
      div_rate = (div_tail + div_head) / 2.d0
    elseif (move_cell > topomove_num) then
      div_head = div_rate
      div_rate = (div_tail + div_head) / 2.d0
    else
      ! 終了
      sense_pre(:) = sense_mid(:)
      print *, 'converge'
      print *, 'movecell fixed: ', move_cell
      return
    end if

    ! 終了条件
    if (roop_count >= roop_max) then
      sense_pre(:) = sense_mid(:)
      print *, 'max loop'
      print *, 'movecell fixed: ', move_cell
      return
    end if

    roop_count = roop_count + 1

  end do

end subroutine Find_new_sense

!-------------------------------------------------------------------------------
! subroutine Dicide_RefVal
! 体積一定削除手法における基準値の決定
!-------------------------------------------------------------------------------
subroutine Dicide_RefVal(sen_val, next_volume, hold_volume, ref_val, err_val)
  implicit none
  real(8), dimension(:) :: sen_val
  integer, intent(in) :: next_volume, hold_volume
  real(8), intent(out) :: ref_val
  real(8), intent(in) :: err_val

  real(8) :: max, sen_x
  integer :: range

  integer :: i, j, k

  range = size(sen_val(:))

  do i=1,range-1
    max=sen_val(i)
    k=i
    do j=i+1,range
      if(sen_val(j) > max)then
        max=sen_val(j)
        k=j
      end if
    end do
    sen_x=sen_val(i)
    sen_val(i)=max
    sen_val(k)=sen_x
  end do
  print *, 'next_volume, hold_volume : ', next_volume, hold_volume
  ref_val = sen_val(next_volume - hold_volume) + err_val

end subroutine Dicide_RefVal

!-------------------------------------------------------------------------------
! subroutine make_Be
! 設計変数配列Beを要素の敏感数と基準値をもとに生成
!-------------------------------------------------------------------------------
subroutine make_Be(ref_val, el_senVal)
  use BESO_DATA
  implicit none
  real(8), intent(in) :: ref_val
  real(8), dimension(:), intent(in) :: el_senVal

  integer :: i

  ! 形態創生の対象となる要素のみBeを更新
  do i = 1, El
    if (Cre_Map(i) == 1) then
      if (ref_val < el_senVal(i)) then
        Be(i) = 1
      else
        Be(i) = 0
      end if
    end if
  end do
end subroutine make_Be

!-------------------------------------------------------------------------------
! subroutine DataArray
! 解析によって得られるデータを格納する配列を用意
!-------------------------------------------------------------------------------
subroutine DataArray
  use BESO_DATA
  implicit none

  allocate(vonMises(El,totalAnalyCase));  vonMises(:,:) = 0.d0
  allocate(dispSense(El,totalAnalyCase)); dispSense(:,:) = 0.d0
  allocate(buckSense(El,totalAnalyCase)); buckSense(:,:) = 0.d0
  allocate(Daylight_fac_sense(El,totalAnalyCase)); Daylight_fac_Sense(:,:) = 0.d0
  
  if (not(BsenJoinNum==0 .and. buckAnalyNum==0)) then
    allocate(buckSense_list(El, BsenJoinNum, totalAnalyCase)); buckSense_list(:,:,:) = 0.d0
    allocate(buckLoad(BsenJoinNum, totalAnalyCase)); buckLoad(:,:) = 0.d0
  end if

  ! 最適化対象の感度
  allocate(Sensitivity(El))

  ! 節点値化した感度
  allocate(Nd_sen(Nd))

  ! フィルター後の要素感度
  allocate(El_sen(El))

  ! 感度係数の２次微分
  allocate(diff_sense(El))

end subroutine DataArray

subroutine Gen_Linear_Filter(filter)
  real(8), intent(out) :: filter(:,:,:)
  integer :: filter_width_x, filter_width_y, filter_width_z
  real(8) :: i_rad

  integer :: ix, iy, iz
  integer :: ind_x, ind_y, ind_z

  integer :: i, j

  filter_width_x = filter_size_x / 2
  filter_width_y = filter_size_y / 2
  filter_width_z = filter_size_z / 2
  filter(:,:,:) = 0.d0

  ind_z = 0
  do iz = -filter_width_z , filter_width_z
    ind_z = ind_z + 1
    ind_y = 0
    do iy = -filter_width_y , filter_width_y
      ind_y = ind_y + 1
      ind_x = 0
      do ix = -filter_width_x, filter_width_x
        ind_x = ind_x + 1
        i_rad = ((iz*hz)**2.d0 + (iy*hy)**2.d0 + (ix*hx)**2.d0)**0.5d0
        if (i_rad > r_min1) cycle
        filter(ind_x, ind_y, ind_z) = -1.d0 / (r_min1*r_min1) * i_rad + 1.d0 / r_min1
        ! filter(ind_x, ind_y, ind_z) = exp(i_rad**2.d0 / (-2.d0 * (r_min1*2.d0)**2.d0)) &
        !   & / (2.d0 * pi * (r_min1*2.d0)**2.d0)

      end do
    end do
  end do

  filter(:,:,:) = filter(:,:,:) / sum(filter(:,:,:))

  ! open(10, file='filter_out.txt', action='write')
  ! do i = 1, filter_size_z
  !   do j = 1, filter_size_y
  !     write(10,'(<filter_size_x>(E15.7,1X))') filter(:,j,i)
  !   end do
  !   write(10,*)
  ! end do
  ! close(10)

end subroutine Gen_Linear_Filter

!--------------------------------------
! Laplacian of Gaussian filter
subroutine Gen_LoG_Filter(filter)
  real(8), intent(out) :: filter(:,:,:)
  integer :: filter_width_x, filter_width_y, filter_width_z
  real(8) :: i_rad

  integer :: ix, iy, iz
  integer :: ind_x, ind_y, ind_z

  filter_width_x = filter_size_x / 2
  filter_width_y = filter_size_y / 2
  filter_width_z = filter_size_z / 2
  filter(:,:,:) = 0.d0

  ind_z = 0
  do iz = -filter_width_z , filter_width_z
    ind_z = ind_z + 1
    ind_y = 0
    do iy = -filter_width_y , filter_width_y
      ind_y = ind_y + 1
      ind_x = 0
      do ix = -filter_width_x, filter_width_x
        ind_x = ind_x + 1
        i_rad = ((iz*hz)**2.d0 + (iy*hy)**2.d0 + (ix*hx)**2.d0)**0.5d0
        if (i_rad > r_min1) cycle
        filter(ind_x, ind_y, ind_z) = exp(i_rad**2.d0 / (-2.d0 * (r_min1*2.d0)**2.d0)) &
          & * (i_rad**2.d0 - 2.d0 * (r_min1*2.d0)**2.d0) / ((2.d0 * pi)**1.5d0 * (r_min1*2.d0)**7.d0)
      end do
    end do
  end do

  filter(:,:,:) = filter(:,:,:) / sum(filter(:,:,:))

end subroutine Gen_LoG_Filter

!--------------------------------
! 空間についての微分値の取得
subroutine Cal_Sense_diff
  real(8) :: diff_filterMat(filter_size_x,filter_size_y,filter_size_z)

  call Gen_LoG_Filter(diff_filterMat(:,:,:))

  call filter_scheme(Sensitivity(:), diff_sense(:), diff_filterMat(:,:,:), &
  & filter_size_x / 2, filter_size_y / 2, filter_size_z / 2)

end subroutine Cal_Sense_diff

end module BESO_ROUTINE
