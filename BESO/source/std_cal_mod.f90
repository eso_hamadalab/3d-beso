module STD_CAL
  implicit none
contains

!-------------------------------------------------------------------------------
! function m_any
! list_aと、list_bに一つでもかぶりがあれば検出
!-------------------------------------------------------------------------------
function m_any(list_a, list_b)
  implicit none
  integer, intent(in) :: list_a(:), list_b(:)
  logical :: m_any

  integer :: i

  do i = 1, size(list_b(:))
    if (any(list_a(:)==list_b(i))) then
      ! リストAとBは、要素重複あり
      m_any = .TRUE.
      return
    end if
  end do
  m_any = .FALSE.
end function m_any

!-------------------------------------------------------------------------------
! function de_array
! 長さ１の1次元配列をスカラーに変換する。
!-------------------------------------------------------------------------------
function de_array(array)
  implicit none
  integer :: de_array
  integer,intent(in) :: array(:)
  de_array = array(1)
end function de_array

!-------------------------------------------------------------------------------
! 配列を標準正規分布に変換して返す。
! subroutine stdNorm_dtb
!-------------------------------------------------------------------------------
subroutine stdNorm_dtb(Vec_A, normdVec_A, in_ave_A, in_stdist_A)
  real(8), dimension(:), intent(in) :: Vec_A
  real(8), dimension(:), intent(out) :: normdVec_A
  real(8), optional, intent(in) :: in_ave_A, in_stdist_A
  real(8) :: ave_A, stdist_A


  if (present(in_ave_A)) then
    ave_A = in_ave_A
  else
    ave_A = sum(Vec_A) / size(Vec_A)
  end if

  if (present(in_stdist_A)) then
    stdist_A = in_stdist_A
  else
    stdist_A = std(Vec_A)
  end if

  normdVec_A(:) = (Vec_A(:) - ave_A) / stdist_A

end subroutine stdNorm_dtb

!-------------------------------------------------------------------------------
! １次元（ベクトル）の任意長のベクトルのノルムを求める。倍精度小数点の配列のみ対応
subroutine norm_sub(nd, Vec_A, norm_A)
!-------------------------------------------------------------------------------
  integer, intent(in) :: nd
  real(8), dimension(:), intent(in) :: Vec_A
  real(8), intent(out) :: norm_A

  real(8) :: sum_square
  integer :: i

  sum_square = 0.d0
  do i = 1, nd
    sum_square =sum_square + Vec_A(i)**2
  end do
  norm_A = sqrt(sum_square)

end subroutine norm_sub

!-------------------------------------------------------------------------------
subroutine skymat_vec_mul(nd, ind, skymat_A, Vec_B, Vec_C)
!	[skymat_vec_mul]
!	マトリクス(1次元配列)とベクトルの乗算 (Vec_B,Vec_Cはベクトル)
! skymat_A(in)とVec_B(in)の乗算。Vec_C(out)が答え。
!-------------------------------------------------------------------------------
	integer, intent(in) :: nd
	integer(8), dimension(:), intent(in) :: ind

	real(8), dimension(:), intent(in) :: skymat_A
	real(8), dimension(:), intent(in) :: Vec_B
	real(8), dimension(:), intent(out) ::Vec_C

	integer :: i,j,k,ii,jj,kk,number,count
	integer :: jfirst,jwidth,kfirst,kwidth,kkwidth,jcount
	real(8) :: s,s_line,s_row

	Vec_C = 0.0d0
	do i=1,nd
		!	行方向
		s_line = 0.0d0
		if(i /= 1) then
			jwidth = ind(i) - ind(i-1)
		else
			jwidth = 1
		end if
		jfirst = i - jwidth + 1
		jcount = 0
		do j=jfirst,i
			jcount = jcount + 1

			if(i /= 1) then
				jj = ind(i-1) + jcount
			else
				jj = jcount
			end if
			s_line = s_line + skymat_A(jj) * Vec_B(j)
		end do

		!	列方向
		s_row = 0.0d0
		do k=i+1,nd
			kwidth = ind(k) - ind(k-1)
			kfirst = k - kwidth + 1
			if(kfirst <= i) then
				kkwidth = i - kfirst + 1
				kk = ind(k-1) + kkwidth
				s_row = s_row + skymat_A(kk) * Vec_B(k)
			end if
		end do
		s = s_line + s_row
		Vec_C(i) = s
	end do

end subroutine skymat_vec_mul

!-------------------------------------------------------------------------------
! listからvalに一致する値を二分探索で見つけ出す。
integer function valloc(list, val)
!-------------------------------------------------------------------------------
  integer, intent(in) :: val
  integer, dimension(:), intent(in) :: list

  integer :: search_loc, left, right

  left = 0
  right = size(list(:))
  head : do
    search_loc = (left + right) / 2
    if (list(search_loc) == val) then
      valloc = search_loc
      return
    elseif (right < left) then
      valloc = 0
      return
    elseif (list(search_loc) < val) then
      left = search_loc + 1
    elseif (list(search_loc) > val) then
      right = search_loc
    end if
  end do head
end function valloc

!-------------------------------------------------------------------------------
! function std
! 倍精度浮動小数点型の１次元配列の標準偏差を求める。
!-------------------------------------------------------------------------------
real(8) function std(value_list)
  implicit none
  real(8), dimension(:), intent(in) :: value_list

  integer :: list_length
  real(8) :: meanVal

  list_length = size(value_list)
  meanVal = sum(value_list) / list_length

  std = sqrt(sum((value_list(:) - meanVal)**2)/list_length)
  return
end function std

!-------------------------------------------------------------------------------
! function vari
! 倍精度浮動小数点型の１次元配列の標準偏差を求める。
!-------------------------------------------------------------------------------
real(8) function vari(value_list)
  implicit none
  real(8), dimension(:), intent(in) :: value_list

  integer :: list_length
  real(8) :: meanVal

  list_length = size(value_list)
  meanVal = sum(value_list) / list_length

  vari = sum((value_list(:) - meanVal)**2)/list_length
  return
end function vari

!-------------------------------------------------------------------------------
! function iqsort
! int型の1次元配列をソート
!-------------------------------------------------------------------------------
recursive function iqsort(ix) result(ires)
  implicit none
  integer, allocatable :: ires(:)
  integer, intent(in)  :: ix(:)
  integer :: k
  allocate(ires(size(ix)))
  if (size(ix) <= 1) then
    ires = ix
  else
    k = ix(size(ix) / 2)
    ires = [iqsort(pack(ix, ix < k)), pack(ix, ix == k), iqsort(pack(ix, ix > k))]
  end if
end function iqsort

!-------------------------------------------------------------------------------
! function dqsort
! int型の1次元配列をソート
!-------------------------------------------------------------------------------
recursive function dqsort(ix) result(ires)
  implicit none
  real(8), allocatable :: ires(:)
  real(8), intent(in)  :: ix(:)
  real(8) :: k
  allocate(ires(size(ix)))
  if (size(ix) <= 1) then
    ires = ix
  else
    k = ix(size(ix) / 2)
    ires = [dqsort(pack(ix, ix < k)), pack(ix, ix == k), dqsort(pack(ix, ix > k))]
  end if
end function dqsort

end module STD_CAL
