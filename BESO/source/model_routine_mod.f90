module model_routines
  use MODEL_MOD
contains
!-------------------------------------------------------------------------------
! subroutine ExternalForce
! 外力ベクトルを設定(自重がある場合設定)
!-------------------------------------------------------------------------------
subroutine ExternalForce
  use PHYSICAL_DATA
  implicit none
  integer :: i, j, k
  integer :: node_i, dof_k
  force(:) = 0.d0
  do i = 1, 3
    do j = 1, MDL_loadNum(i)
      force((MDL_loadNode(j,i)-1)*3+i) = MDL_loadVal(j,i)
    end do
  end do

  ! 自重の設定
  if(grav_weight==0) return
  do i = 1, MDL_elNum
    do j = 1, 8
      node_i = indv(i,j)
      do k = 1, 3
        dof_k = (node_i-1)*3 + k
        if (k==abs(gravityDir)) then
          force(dof_k) = force(dof_k) + unitVolume*density_list(mte(i))*sign(1,gravityDir)/8
        end if
      end do
    end do
  end do
end subroutine ExternalForce

!=====================================================================
                  subroutine strain_energy(F_vec, U_vec)
!                     ひずみエネルギーの計算
!=====================================================================
  use MATRIX_SOLVER
  use PHYSICAL_DATA, only: unitVolume
  implicit none
  real(8), intent(in) :: F_vec(:), U_vec(:)

  integer :: i,j

  !=====外力項=====
  U_energyCase_i = 0.d0
  do i = 1,lc
    U_energyCase_i = U_energyCase_i + F_vec(i) * U_vec(i) * 0.5d0
  end do

  !=====内力項=====
  V_energyCase_i = 0.d0
  do i = 1, MDL_elNum
    do j = 1,6
      V_energyCase_i = V_energyCase_i + stress(i,j) * ep(6*(i - 1) + j) * unitVolume
    end do
  end do
  V_energyCase_i = V_energyCase_i / 2.d0

end subroutine strain_energy

!===============================================================================
! subroutine strain
!  要素中心のひずみの計算 {ε}=[B]{δ}
!===============================================================================
subroutine strain
  use StiffMat_GenMOD
  integer :: i,j,k,l,jj
  integer,dimension(24) :: list
  real(8) :: xi,eta,zeta,detJ
  real(8),dimension(8) :: shape_n,dndxi,dndeta,dndzeta
  real(8),dimension(24) :: edisp
  real(8),dimension(3,3) :: Jinv

  do i=1, MDL_elNum

    xi = 0.0d0
    eta = 0.0d0
    zeta = 0.0d0

    !  形状関数の微分
    call dshape_function(xi,eta,zeta,dndxi,dndeta,dndzeta)
    !  ヤコビ行列
    call make_Jmatrix(i,dndxi,dndeta,dndzeta,detJ,Jinv)
    !  ひずみ-変位マトリクス
    call make_Bmatrix(dndxi,dndeta,dndzeta,Jinv)

    !  変位の対応表を作成
    do j=1,8
      do k=1,3
        list(3*(j-1)+k) = 3*(indv(i,j)-1) + k
      end do
    end do

    do k=1,6
      ep(6*(i-1)+k) = 0.0d0
      do l=1,24
        jj = list(l)
        ep(6*(i-1)+k) = ep(6*(i-1)+k) + Bmatrix(k,l) * disp(jj)
      end do
    end do

  end do

  ! do i=1,MDL_elNum
  !   write(107,'(i3,6e15.5)')i,ep(6*(i-1)+1),ep(6*(i-1)+2),ep(6*(i-1)+3),ep(6*(i-1)+4),ep(6*(i-1)+5),ep(6*(i-1)+6)
  ! end do
end subroutine strain

!===============================================================================
! subroutine stress_get
!                要素中心の応力の計算 {σ}=[D]{ε}
!===============================================================================
subroutine stress_get
  use StiffMat_GenMOD
  integer :: i, j, k

  stress(:,:) = 0.0d0
  do i=1,MDL_elNum
    !  応力-ひずみマトリクス
    call make_Dmatrix(i)
    do j=1,6
      do k=1,6
        stress(i,j) = stress(i,j) + Dmatrix(j,k) * ep(6*(i-1)+k)
      end do
    end do
  end do

end subroutine stress_get

!-------------------------------------------------------------------------------
! subroutine Principal_Stress
! 主応力求める。
!-------------------------------------------------------------------------------
subroutine Principal_Stress
  real(8) :: stressTensor(3,3)
  real(8) :: work(10)
  integer :: info
  open(7,file='./dataout/analysis/pstressVec.txt',action='write')
  do i = 1, MDL_elNum
    ! テンソルの下三角に格納
    stressTensor(1,1) = stress(i,1)
    stressTensor(2,2) = stress(i,2)
    stressTensor(3,3) = stress(i,3)

    stressTensor(2,1) = stress(i,4)
    stressTensor(3,1) = stress(i,5)
    stressTensor(3,2) = stress(i,6)

    call dsyev('V', 'L', 3, stressTensor, 3, pStress(i,1:3), work, 10, info)
    if (info/=0) then
      print *, 'error occure at dsyev in Principal_Stress:'
      print *, 'error code : ', info
    end if

    pStressVec(i,1,1:3) = stressTensor(1:3,1)
    pStressVec(i,2,1:3) = stressTensor(1:3,2)
    pStressVec(i,3,1:3) = stressTensor(1:3,3)
    write(7,'(i7,9e15.5)') i,pStress(i,1),pStress(i,2),pStress(i,3)

  end do

end subroutine Principal_Stress

!=====================================================================
                    subroutine Cal_VonMises
!                Von Mises応力の計算
!=====================================================================
  use STD_CAL
  implicit none
  integer :: i

  !要素のVon Mises応力を求める。
  do i=1,MDL_elNum
    MDL_vonMises(i)=sqrt(((stress(i,1)-stress(i,2))**2+(stress(i,2)-stress(i,3))**2+(stress(i,3)-stress(i,1))**2+6.0D0*(stress(i,4)**2+stress(i,5)**2+stress(i,6)**2))/2.0D0)
  end do

  ! 統計量の算出
  vonMax  = maxval(pack(MDL_vonMises(:),mte(:)==1))
  vonMin  = minval(pack(MDL_vonMises(:),mte(:)==1))
  vonMean = sum(pack(MDL_vonMises(:),mte(:)==1)) / count(mte(:)==1)
  vonStd  = std(pack(MDL_vonMises(:),mte(:)==1))

end subroutine Cal_VonMises

!-------------------------------------------------------------------------------
! subroutine sus_Buck_sense
! 座屈敏感数を求める。
!-------------------------------------------------------------------------------
subroutine sus_Buck_sense(mix_mode, eigen_vector, gkt_CSR)
  use GeoMat_GenMOD
  use StiffMat_GenMOD
  use STD_CAL
  use MATRIX_SOLVER
  implicit none
  integer, intent(in) :: mix_mode
  real(8), intent(in) :: eigen_vector(:,:), gkt_CSR(:)

  real(8) :: el_mode_Vec(24,mix_mode)
  real(8) :: weight_bottom
  real(8) :: rayleigh_bottom(mix_mode), rayleigh_top(mix_mode)
  real(8) :: penalty = 1.d0

  integer :: full_ind, lc_ind
  real(8) :: tmp_Vec_A(lc), tmp_Vec_B(24), tmp_Mat_E(24,24)
  integer :: i, j, k, ie

  MDL_buckSense_list(:,:) = 0.d0

  do i = 1, mix_mode
    call mkl_dcsrsymv('U', lc, gkt_CSR, CSR_rowIndex, CSR_columns, &
      & eigen_vector(:,i), tmp_Vec_A(:))
    rayleigh_bottom(i) = dot_product(tmp_Vec_A(:), eigen_vector(:,i))
  end do

  do ie = 1, MDL_elNum
    el_mode_Vec(:,:) = 0.d0
    do i = 1, mix_mode
      do j = 1, 8
        do k = 1, 3
          full_ind = (indv(ie,j) - 1) * 3 + k
          lc_ind = icrs(full_ind)
          if(lc_ind == 0) cycle
          el_mode_Vec((j - 1) * 3 + k,i) = eigen_vector(lc_ind,i)
        end do
      end do
    end do

    ! 要素剛性マトリクス    : em
    ! 要素幾何剛性マトリクス : em0
    call mkStiff_elMat(ie)
    call mkGeo_elMat(ie)

    do i = 1, mix_mode
      tmp_Mat_E(:,:) = eigen_value(i) * em0(:,:) - em(:,:)
      call dsymv('U', 24, 1.d0, tmp_Mat_E(1:24,1:24), 24, el_mode_Vec(:,i), 1, &
        & 0.d0, tmp_Vec_B(1:24), 1)
      rayleigh_top(i) = dot_product(el_mode_Vec(:,i), tmp_Vec_B(:))
    end do

    do i = 1, mix_mode
      MDL_buckSense_list(ie,i) = rayleigh_top(i) / rayleigh_bottom(i)
    end do

  end do

  ! 統計量の算出
  do i = 1, MDL_BsenJoinNum
    BsenMax(i)  = maxval(pack(MDL_buckSense_list(:,i), mte(:)==1))
    BsenMin(i)  = minval(pack(MDL_buckSense_list(:,i), mte(:)==1))
    BsenMean(i) = sum(pack(MDL_buckSense_list(:,i), mte(:)==1)) / count(mte(:)==1)
    BsenStd(i)  = std(pack(MDL_buckSense_list(:,i), mte(:)==1))
  end do

end subroutine sus_Buck_sense

!-------------------------------------------------------------------------------
! subroutine Cal_dispSense
! 要素中心の変位敏感数の計算
!-------------------------------------------------------------------------------
subroutine Cal_dispSense
  use STD_CAL
  use StiffMat_GenMOD
  implicit none

  integer :: n1,ie,i,j,k
  real(8) :: dl_sns
  real(8),dimension(24) :: disp_sum,delta

  MDL_dispSense(:)=0.0d0

  do ie=1,MDL_elNum
    k=0
    dl_sns=0.d0
    disp_sum=0.d0
    delta=0.d0

    call mkStiff_elMat(ie) !要素剛性マトリクスの作成

    ! 要素を構成する節点番号の変位を求める
    do i=1,8
      do j=1,3
        k=k+1
        n1=3*(indv(ie,i)-1)+j
        delta(k)=disp(n1)
      end do
    end do

    do i=1,24
      do j=1,24
        disp_sum(j)=disp_sum(j)+delta(i)*em(i,j)
      end do
    end do

    do i=1,24
      dl_sns=dl_sns+delta(i)*disp_sum(i)
    end do

    MDL_dispSense(ie)=dabs(dl_sns)
  end do

  ! 統計量の算出
  DsenMax  = maxval(pack(MDL_dispSense(:),mte(:)==1))
  DsenMin  = minval(pack(MDL_dispSense(:),mte(:)==1))
  DsenMean = sum(pack(MDL_dispSense(:),mte(:)==1)) / count(mte(:)==1)
  DsenStd  = std(pack(MDL_dispSense(:),mte(:)==1))

end subroutine Cal_dispSense

!-------------------------------------------------------------------------------
! [subroutine mirror]
! 真の自由度を総自由度に転写する。変位や、モードなんか。
!-------------------------------------------------------------------------------
subroutine mirror(icrs, nume_flx_Vec, full_flx_Vec)
  implicit none
  integer, dimension(:), intent(in) :: icrs
  real(8), dimension(:), intent(in) :: nume_flx_Vec
  real(8), dimension(:), intent(out) :: full_flx_Vec

  integer :: i

  full_flx_Vec(:) = 0.d0
  do i = 1, size(icrs(:))
    if (icrs(i) /= 0) then
      full_flx_Vec(i) = nume_flx_Vec(icrs(i))
    end if
  end do
end subroutine mirror

end module model_routines
