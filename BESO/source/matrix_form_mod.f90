module MATRIX_FORM
  use MODEL_MOD
  implicit none
  type list
    integer :: val
    type(list), pointer :: next
  end type list
  type ptr
    type(list), pointer :: p
  end type ptr
contains
!-------------------------------------------------------------------------------
! subroutine assemSkyMat
! skyMat_Aに対して、el_ID番目の要素剛性マトリクスを、
! マトリクス構造に合わせて足し合わせる。
! 16/10/01 @ yamazaki
! *-input-*
! el_ID    : 要素番号
! skyMat_A : skylineマトリクス
! el_Mat   : 要素マトリクス
! *-output-*
! skyMat_A : skylineマトリクス
!-------------------------------------------------------------------------------
subroutine assemSkyMat(el_ID, skyMat_A, el_Mat)
  use MATRIX_SOLVER
  implicit none
  integer, intent(in) :: el_ID
  real(8), intent(in) :: el_Mat(24,24)
  real(8), intent(inout) :: skyMat_A(:)

  integer :: i, j, ip, nt, it, jt, jp, kk, iel, jel

  do ip=1,8
    do i=1,3
      nt = 3*(indv(el_ID,ip)-1) + i
      it=icrs(nt)
      if(it==0)cycle
      do jp=1,8
        do j=1,3
          nt = 3*(indv(el_ID,jp)-1) + j
          jt = icrs(nt)
          if(jt==0) cycle
          if(it>jt) cycle
          kk = ind(jt) + it - jt
          iel = 3*(ip-1) + i
          jel = 3*(jp-1) + j
          skyMat_A(kk) = skyMat_A(kk) + el_Mat(iel,jel)
        end do
      end do
    end do
  end do

end subroutine assemSkyMat

!-------------------------------------------------------------------------------
! subroutine assemSkyMat
! CSRMat_Aに対して、el_ID番目の要素剛性マトリクスを、
! マトリクス構造に合わせて足し合わせる。
! 16/10/01 @ yamazaki
! *-input-*
! el_ID    : 要素番号
! CSRMat_A : skylineマトリクス
! el_Mat   : 要素マトリクス
! *-output-*
! CSRMat_A : CSRマトリクス
!-------------------------------------------------------------------------------
subroutine assemCSRMat(el_ID, CSRMat_A, el_Mat)
  use MATRIX_SOLVER
  implicit none
  integer, intent(in) :: el_ID
  real(8), intent(in) :: el_Mat(24,24)
  real(8), intent(inout) :: CSRMat_A(:)

  integer :: lo(24)
  integer :: row_head, row_tail, locij

  integer :: i, j

  ! el_ID要素の自由度を羅列
  call locate(el_ID,lo(:),8,3)

  do i = 1, 24
    if(lo(i) == 0) cycle
    row_head = CSR_rowIndex(lo(i))
    row_tail = CSR_rowIndex(lo(i)+1)-1
    do j = 1, 24
      ! 拘束節点
      if(lo(j) == 0) cycle
      ! 下三角
      if(lo(j) < lo(i)) cycle
      locij = minloc(CSR_columns(row_head:row_tail), 1, CSR_columns(row_head:row_tail)==lo(j))
      CSRMat_A(row_head+locij-1) = CSRMat_A(row_head+locij-1) + el_Mat(i,j)
    end do
  end do

end subroutine assemCSRMat

!===============================================================================
subroutine arange
!	拘束節点自由度を取り除いた真の自由度の番号付け
!===============================================================================
  use MATRIX_SOLVER
	integer :: n3,i,j,m,nt,nc

	n3 = MDL_nodeNum * 3
	icrs(:) = 1

	do i=1,3
		m = nfix(i)
		if(m /= 0) then
			do j=1,m
				nt = 3 * (nfixp(j,i) - 1) + i
				icrs(nt) = 0
			end do
		end if
	end do

	lc = 0
	do i=1,MDL_nodeNum
		do j=1,3
			nt = 3 * (i - 1) + j
			nc = icrs(nt)
			if(nc /= 0) then
				lc = lc + 1
				icrs(nt) = lc
			end if
		end do
	end do

end subroutine

!===============================================================================
subroutine mhight(ns,nf)
!	スカイライン法により連立方程式を解くにあたって、全体剛性マトリクスの非零成分の配置特性の調査
! indの作成
!===============================================================================
  use STD_CAL
  use MATRIX_SOLVER
  implicit none
	integer,intent(in) :: ns,nf
	integer,dimension(ns*nf) :: lo
  integer :: ne_min
	integer :: nsf,ne,i,it,jh

	ind(:) = 0
	nsf = ns * nf
	do ne=1,MDL_elNum
		call locate(ne,lo,ns,nf)
    lo(:) = iqsort(lo(:))
    ! 最小の自由度を探索
    ne_min = count(lo(:)==0) + 1

    ! 高さの算出
    do i = ne_min, nsf
			it = lo(i)
			jh = it - lo(ne_min)
			if(jh > ind(it)) ind(it) = jh
		end do
	end do

  ind(1) = ind(1) + 1
	do i = 2, lc
    ind(i) = ind(i-1) + ind(i) + 1
	end do

end subroutine mhight

!===============================================================================
subroutine mwidth(ns,nf)
!	スカイライン法により連立方程式を解くにあたって、全体剛性マトリクスの非零成分の配置特性の調査
! ind_rowの作成
!===============================================================================
  use STD_CAL
  use MATRIX_SOLVER
  implicit none
	integer,intent(in) :: ns,nf
	integer :: lo(ns*nf)
  integer :: ne_min
	integer :: nsf,ne,i,it,jh

	ind_row(:) = 0
	nsf = ns * nf
	do ne=1,MDL_elNum
		call locate(ne,lo,ns,nf)
    lo(:) = iqsort(lo(:))
    ! 最小の自由度を探索
    ne_min = count(lo(:)==0) + 1

    ! 幅算出
    do i = ne_min, nsf
			it = lo(i)
			jh = lo(nsf) - it
			if(jh > ind_row(it)) ind_row(it) = jh
		end do
	end do

  ind_row(1) = ind_row(1) + 1
	do i = 2, lc
    ind_row(i) = ind_row(i-1) + ind_row(i) + 1
	end do

end subroutine mwidth

!-------------------------------------------------------------------------------
! subroutine MkCSR_Index
! CSR formatを生成
!-------------------------------------------------------------------------------
subroutine MkCSR_Index
  use MATRIX_SOLVER
  integer :: lo(24)
  type(ptr), allocatable :: root(:)
  integer, allocatable :: len_list(:)
  integer :: row_head, row_tail

  integer :: i, j

  allocate(root(lc), len_list(lc))
  len_list(:) = 1
  do i = 1, lc
    allocate(root(i)%p)
    allocate(root(i)%p%next)
    root(i)%p%val = i
    nullify(root(i)%p%next%next)
  end do

  ! Map CSR format to lists
  do i = 1, MDL_elNum
    call locate(i, lo(:), 8, 3)
    do j = 1, 24
      if (lo(j)==0) cycle
      call add_list(root(lo(j))%p, pack(lo(:), lo(:)>lo(j)), len_list(lo(j)))
    end do
  end do
  CSRMat_len = sum(len_list(:))

  ! CSR_rowIndex
  CSR_rowIndex(1) = 1
  do i = 2, lc+1
    CSR_rowIndex(i) = CSR_rowIndex(i-1) + len_list(i-1)
  end do

  ! CSR_columns
  allocate(CSR_columns(CSRMat_len))
  do i = 1, lc
    row_head = CSR_rowIndex(i)
    row_tail = CSR_rowIndex(i+1)-1
    call list_to_array(root(i)%p, CSR_columns(row_head:row_tail))
  end do

  do i = 1, lc
    call del_list(root(i)%p)
  end do
  deallocate(root)

  end subroutine MkCSR_Index

  !--- print list
  subroutine write_list(list_head)
    type(list), pointer :: list_head
    type(list), pointer :: work

    work => list_head
    do
      if(associated(work%next)) then
        print *, work%val
        work => work%next
      else
        exit
      end if
    end do
  end subroutine write_list

  !--- add array components to list
  subroutine add_list(list_head, add_array, len)
    type(list), pointer :: list_head
    integer, intent(in) :: add_array(:)
    integer, intent(inout) :: len
    type(list), pointer :: work, temp

    integer :: i
    do i = 1, size(add_array(:))
      work => list_head
      do
        if (add_array(i) <= work%val) exit
        if (associated(work%next%next)) then
          if (add_array(i) < work%next%val) then
            allocate(temp)
            temp%next => work%next
            temp%val = add_array(i)
            work%next => temp
            len = len + 1
            exit
          else
            work => work%next
            cycle
          end if
        else
          allocate(temp)
          temp%next => work%next
          temp%val = add_array(i)
          work%next => temp
          len = len + 1
          exit
        end if
      end do
    end do
  end subroutine add_list

  !--- copy list to array
  subroutine list_to_array(list_head, array)
    type(list), pointer :: list_head
    integer, intent(out) :: array(:)

    type(list), pointer :: work
    integer :: ind
    work => list_head
    ind = 0
    do
      if(associated(work%next)) then
        ind = ind + 1
        array(ind) = work%val
        work => work%next
      else
        exit
      end if
    end do

  end subroutine list_to_array

  !--- deleate list
  subroutine del_list(list_head)
    type(list), pointer :: list_head
    type(list), pointer :: work, next_work

    work => list_head
    do
      if(associated(work%next)) then
        next_work => work%next
        deallocate(work)
        work => next_work
      else
        exit
      end if
    end do
  end subroutine del_list

!-------------------------------------------------------------------------------
! subroutine MakeCSR_RowSearch_Array
! CSRの一行の要素数を把握する。
!-------------------------------------------------------------------------------
subroutine MakeCSR_RowSearch_Array
  use STD_CAL
  use MATRIX_SOLVER
  implicit none

  integer, parameter :: max_array = 100
  integer :: stack_num, i_DOF_index(3), lo(24)
  integer, allocatable :: search_stack(:), node_share_element(:,:), node_share_elmNum(:)

  integer :: i, j, k

  allocate(search_stack(max_array))
  open(10, status='scratch', form='unformatted')

  allocate(node_share_element(MDL_nodeNum,9), node_share_elmNum(MDL_nodeNum))
  node_share_element(:,:) = 0
  node_share_elmNum(:) = 0

  do i = 1, MDL_elNum
    do j = 1, 8
      node_share_elmNum(indv(i,j)) = node_share_elmNum(indv(i,j)) + 1
      node_share_element(indv(i,j),node_share_elmNum(indv(i,j))) = i
    end do
  end do

  do i = 1, MDL_nodeNum
    ! TODO:この節点の自由度がすべて真の自由度になかったら探索する意味が無いのでスキップ
    do j = 1, 3
      i_DOF_index(j) = icrs((i-1)*3+j)
    end do
    if (all(i_DOF_index(:)==0)) cycle

    ! 各節点が各要素に含まれるかをチェック
    stack_num = 0
    do j = 1, node_share_elmNum(i)

      ! j要素はi節点を含む→j要素の自由度をリストに追加
      if (all(indv(node_share_element(i,j),:) /= i)) cycle
      call locate(node_share_element(i,j),lo,8,3)
      do k = 1, 24
        if (lo(k)==0) cycle
        if (any(lo(k)==search_stack(1:stack_num))) cycle

        stack_num = stack_num + 1
        search_stack(stack_num) = lo(k)
      end do
    end do

    do j = 1, 3
      if (i_DOF_index(j)==0) cycle
      ! 上三角のみ採用(行自由度より大きい自由度の幅を確保)
      ind_row(i_DOF_index(j)) = count(search_stack(1:stack_num)>=i_DOF_index(j))

      write(10) iqsort(pack(search_stack(1:stack_num), &
        & search_stack(1:stack_num)>=i_DOF_index(j)))
    end do

  end do

  do i = 2, lc
    ind_row(i) = ind_row(i-1) + ind_row(i)
  end do
  rewind(10)
  allocate(CSR_columns(ind_row(lc)))

  read(10) CSR_columns(1:ind_row(1))
  do i = 2, lc
    read(10) CSR_columns(ind_row(i-1)+1:ind_row(i))
  end do
  close(10)

  CSR_rowIndex(1) = 1
  CSR_rowIndex(2:lc+1) = ind_row(1:lc) + 1
  CSRMat_len = CSR_rowIndex(lc+1) - 1

end subroutine MakeCSR_RowSearch_Array

!-------------------------------------------------------------------------------
! subroutine makeCSR_index
! CSRマトリクス格納形式のindex作成
!-------------------------------------------------------------------------------
subroutine makeCSR_index(ns, nf)
  use STD_CAL
  use MATRIX_SOLVER
  implicit none
  integer, intent(in) :: ns, nf
  integer :: nsf, nel_i, srch_DOF
  integer :: zeroDOF_NUM
  integer :: lo(ns*nf)
  integer, allocatable :: mid_CSRind(:)

  integer :: i_rowHead, i_rowTail, mid_rowHead, mid_rowTail
  integer :: i

  nsf = ns * nf
  allocate(mid_CSRind(ind_row(lc)))
  mid_CSRind(:) = 0
  CSR_rowIndex(:) = 0

  do nel_i = 1, MDL_elNum
    call locate(nel_i, lo(:), ns, nf)
    lo(:) = iqsort(lo(:))
    zeroDOF_NUM = count(lo(:)==0)
    do i = zeroDOF_NUM + 1, nsf
      srch_DOF = lo(i)
      ! mid_CSRind
      if (srch_DOF == 1) then
        call rec_update(lo(i:), &
        & mid_CSRind(1 : ind_row(srch_DOF)), &
        & CSR_rowIndex(srch_DOF))
      else
        call rec_update(lo(i:), &
        & mid_CSRind(ind_row(srch_DOF-1)+1 : ind_row(srch_DOF)), &
        & CSR_rowIndex(srch_DOF))
      end if
    end do
  end do

  ! columns初期設定
  CSRMat_len = sum(CSR_rowIndex(:))
  allocate(CSR_columns(CSRMat_len))
  CSR_columns(:) = 0

  i_rowHead = 1
  i_rowTail = CSR_rowIndex(1)
  do i = 1, lc
    ! columnsを中間配列から転写
    if (i==1) then
      CSR_columns(i_rowHead:i_rowTail) = &
      & iqsort(mid_CSRind(1:CSR_rowIndex(1)))
    else
      CSR_columns(i_rowHead:i_rowTail) = &
      & iqsort(mid_CSRind(ind_row(i-1)+1:ind_row(i-1)+CSR_rowIndex(i)))
    end if

    ! rowIndexの生成
    CSR_rowIndex(i) = i_rowHead
    i_rowHead = i_rowTail + 1
    if (i==lc) then
      CSR_rowIndex(i+1) = i_rowHead
    else
      i_rowTail = i_rowTail + CSR_rowIndex(i+1)
    end if
  end do
  deallocate(mid_CSRind)
end subroutine makeCSR_index

!===============================================================================
subroutine locate(ne,lo,ns,nf)
!	スカイライン法により連立方程式を解くにあたって、全体剛性マトリクスの非零成分の配置特性の調査
! 要素剛性マトリクスに対応する自由度をまとめる。>> lo(ns*nf)
!===============================================================================
  use STD_CAL
  implicit none
	integer, intent(in) :: ne,ns,nf
	integer, intent(out) :: lo(ns*nf)
	integer :: i,ib,n,k
  integer :: i_Node

  ! 総自由度
	do i=1,ns
		ib = nf * (i-1)
		i_Node = indv(ne,i)
		n = nf * (i_Node-1)
		do k=1,nf
			lo(ib+k) = n + k
		end do
	end do

  ! 真の自由度
  do i = 1, ns*nf
    lo(i) = icrs(lo(i))
  end do

end subroutine locate

!-------------------------------------------------------------------------------
! subroutine rec_update
! chk_loのそれぞれの要素について、rec_array(1:rec_num)に
! 登録されていなければ、rec_numを更新し、登録
!-------------------------------------------------------------------------------
subroutine rec_update(chk_lo, rec_array, rec_num)
  implicit none
  integer, intent(in) :: chk_lo(:)
  integer, intent(inout) :: rec_num, rec_array(:)

  integer :: i

  do i = 1, size(chk_lo(:))
    if (any(chk_lo(i)==rec_array(1:rec_num))) then
      cycle
    else
      rec_num = rec_num + 1
      rec_array(rec_num) = chk_lo(i)
    end if
  end do

end subroutine rec_update

end module MATRIX_FORM
