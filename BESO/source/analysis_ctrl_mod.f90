module ANALYSIS_CTRL
  use MODEL_SUPER
  use FILE_IO
  character(len=8) dbleForm
contains
!-------------------------------------------------------------------------------
! subroutine Analysis_All
! 各解析ケースについて解析
!-------------------------------------------------------------------------------
subroutine Analysis_All
  use model_routines
  use MODEL_MOD
  use BESO_DATA
  use MATRIX_FORM
  use MATRIX_SOLVER
  use StiffMat_GenMOD
  use GeoMat_GenMOD
  use daylight_factor
  implicit none
  !*-外力*-
  real(8), allocatable :: F_vec(:)
  !*-変位*-
  real(8), allocatable :: U_vec(:)
  real(8), allocatable :: eigen_vector(:,:)

  ! マトリクス
  ! real(8), allocatable :: gkt(:), gkl(:), gk0(:)
  real(8), allocatable :: gk_CSR(:), gkt_CSR(:)

  real(8), parameter :: eps = 1.d-15

  integer :: unchange_sup
  integer :: i

  unchange_sup = 0
  call MDL_DataArray

  do case_i = 1, totalAnalyCase
    write(analyCaseStr,'(I2.2)') case_i

    ! nfixp, nfixの作成
    call Time_Stamp('case' // analyCaseStr // '支持情報の作成')
    call SetSupport(case_i)

    ! MDL_loadNum, MDL_loadNode, MDL_loadValの作成
    call Time_Stamp('case' // analyCaseStr // '荷重情報の作成')
    call SetLoad(case_i)

! ここから、モデルモジュールの変数だけで独立しているはず
!==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-=
    if (unchange_sup==0) then
      ! 支持条件が変わらなければマトリクス構造は同じ
      call arange
      if (allocated(CSR_rowIndex)) deallocate(CSR_rowIndex)
      if (allocated(CSR_columns))  deallocate(CSR_columns)
      ! if (allocated(ind))     deallocate(ind)
      if (allocated(ind_row)) deallocate(ind_row)
      if (allocated(F_vec))   deallocate(F_vec)
      if (allocated(U_vec))   deallocate(U_vec)

      allocate(CSR_rowIndex(lc+1))
      ! allocate(ind(lc))     ; ind(:) = 0
      allocate(ind_row(lc)) ; ind_row(:) = 0
      allocate(F_vec(lc))
      allocate(U_vec(lc))

      ! call Time_Stamp('case' // analyCaseStr // 'skylineマトリクス構造の作成')
      ! call mhight(8,3)

      call Time_Stamp('case' // analyCaseStr // 'CSRマトリクス構造の作成')
      ! call mwidth(8,3)
      ! call MakeCSR_RowSearch_Array
      call MkCSR_Index
      ! call makeCSR_index(8,3)
      if (allocated(ind_row)) deallocate(ind_row)

      call Time_Stamp('case' // analyCaseStr // '各マトリクス配列の割付')
      ! BLAS向けind作成
      ! if (allocated(ind_blas))  deallocate(ind_blas)
      ! allocate(ind_blas(lc+1))
      ! ind_blas(1) = ind(1)
      ! do i = 1, lc
      !   ind_blas(i+1) = ind(i) + 1
      ! end do

      ! if (allocated(gk0))    deallocate(gk0)
      ! if (allocated(gkt))    deallocate(gkt)
      ! if (allocated(gkl))    deallocate(gkl)
      if (allocated(gk_CSR)) deallocate(gk_CSR)
      if (allocated(gkt_CSR))deallocate(gkt_CSR)
      ! allocate(gkt(ind(lc)))
      ! allocate(gkl(ind(lc)))
      allocate(gk_CSR(CSRMat_len))
      if (eigenSolveNum > 0) then
        allocate(gkt_CSR(CSRMat_len))
      end if
    end if

    ! 外力ベクトルの作成
    call ExternalForce
    F_vec(:) = pack(force(:), icrs(:)/=0)

    ! skyline剛性マトリクスの作成
    ! call Time_Stamp('case' // analyCaseStr // 'skyline剛性マトリクスの作成')
    ! call mkSkyStiff_Mat(gkl(:))

    ! CSR剛性マトリクスの作成
    call Time_Stamp('case' // analyCaseStr // 'CSR剛性マトリクスの作成')
    call mkCSRStiff_Mat(8,3, gk_CSR(:))

    !%%%%%%% 線形解析 %%%%%%%
    ! allocate(gk0(ind(lc)))
    ! gk0(:) = gkl(:)
    U_vec(:) = F_vec(:)
    ! call Time_Stamp('case' // analyCaseStr // 'skysolverの実行')
    ! call skysolver(gk0,U_vec,ind,lc)
    ! deallocate(gk0)

    call Time_Stamp('case' // analyCaseStr // 'pardiso solverの実行')
    call pardiso_solver(lc, CSRMat_len, CSR_rowIndex(:), CSR_columns(:), &
    & gk_CSR(:), F_vec(:), U_vec(:))

    !%%%%%%% 線形解析結果による物理量計算 %%%%%%%
    ! 歪・応力・ミーゼス応力・変位敏感数・ひずみエネルギー
    call Time_Stamp('case' // analyCaseStr // '線形解析結果の算出・出力')
    call mirror(icrs, U_vec(:), disp(:))

    call strain
    call stress_get

    call Principal_Stress

    call Cal_VonMises

    call Cal_dispSense

    call strain_energy(F_vec(:), U_vec(:))
    !%%%%%%% 採光解析 %%%%%%%
    !if (kstep == 1) then
    call potential_daylight_factor
    U_val(:)=0
    !else
    call cal_daylight_factor   
    !end if
    
    call output_daylight_factor
    
    call output_varience_val
    
    !%%%%%%% 座屈解析 %%%%%%%
    if (eigenSolveNum > 0) then
      ! call eigen_solve(eigenSolveNum, gkt(:), gk0(:))
      if (allocated(eigen_vector)) deallocate(eigen_vector)
      if (allocated(eigen_value))  deallocate(eigen_value)
      allocate(eigen_vector(lc, eigenSolveNum)); eigen_vector(:,:)=0.d0
      allocate(eigen_value(eigenSolveNum));      eigen_value(:)=0.d0

      ! call Time_Stamp('case' // analyCaseStr // 'skyline幾何剛性マトリクスの作成')
      ! call mkSkyGeo_Mat(gkt(:))
      call Time_Stamp('case' // analyCaseStr // 'CSR幾何剛性マトリクスの作成')
      call mkCSRGeo_Mat(8, 3, gkt_CSR(:))

      ! lapackの固有値解析
      ! call Time_Stamp('case' // analyCaseStr // '三角行列線形座屈解析の実行')
      ! call eigen_math_solver(lc, ind, gkt, gkl, eigenSolveNum, eps,&
      !                           & eigen_value, eigen_vector)
      ! 固有値がinverseされて、返ってくる。
      ! eigen_value(:) = - 1.d0 / eigen_value(:)
      ! do i = 1, size(eigen_value)
      !   print *, eigen_value(i)
      ! end do

      eigen_value(:) = 0.d0

      ! call Time_Stamp('case' // analyCaseStr // 'ARPACK線形座屈解析の実行')
      ! ! 本命の固有値解析
      ! call dsdrv5(lc, ind, ind_blas, gkl, -gkt, eigenSolveNum, &
      !   & eigen_value, eigen_vector)

      call Time_Stamp('case' // analyCaseStr // 'ARPACK_CSR線形座屈解析の実行')
      call dsdrv5_CSR(lc, CSRMat_len, CSR_rowIndex(:), CSR_columns(:), gk_CSR(:), &
        & -gkt_CSR(:), eigenSolveNum, eigen_value, eigen_vector)


      call Time_Stamp('case' // analyCaseStr // '線形座屈解析結果の算出・出力')
      !%%%%%%% 座屈解析結果による感度係数等計算 %%%%%%%
      ! 総自由度への転写
      do i = 1, eigenSolveNum
        call mirror(icrs, eigen_vector(:,i), mode_vectors(:,i))
      end do

      ! 敏感数の算出
      call Time_Stamp('case' // analyCaseStr // '座屈敏感数の算出')
      call sus_Buck_sense(MDL_BsenJoinNum, eigen_vector(:,:), gkt_CSR(:))

    end if

!==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-=
! ここまで、モデルモジュールの変数だけで独立しているはず

    call Time_Stamp('case' // analyCaseStr // 'ケース解析結果の出力')
    !%%%%%%% 各種物理量等,計算結果の設計領域への移動 %%%%%%%
    call DataToBLDArea(case_i)

    !%%%%%%% 解析結果・計算結果のファイル出力 %%%%%%%
    !---ファイル出力---
    call output_analyInfo

    if (eigenSolveNum > 0) then
      call output_eigenInfo
      ! call OutputMode_inp2
    end if

    call output_inp
    call output_vtkfile_ug
    ! call OutputMode_inp

    if (case_i < totalAnalyCase) then
      if (analyCase_list(case_i,1)==analyCase_list(case_i+1,1)) then
        unchange_sup = 1
      else
        unchange_sup = 0
      end if
    end if
  end do

  call Time_Stamp('モデル情報の開放')

  if (allocated(CSR_rowIndex)) deallocate(CSR_rowIndex)
  if (allocated(CSR_columns))  deallocate(CSR_columns)
  ! if (allocated(ind))     deallocate(ind)
  if (allocated(ind_row)) deallocate(ind_row)
  if (allocated(F_vec))   deallocate(F_vec)
  if (allocated(U_vec))   deallocate(U_vec)

  if (allocated(eigen_vector)) deallocate(eigen_vector)
  if (allocated(eigen_value))  deallocate(eigen_value)

  ! if (allocated(gk0))    deallocate(gk0)
  ! if (allocated(gkt))    deallocate(gkt)
  ! if (allocated(gkl))    deallocate(gkl)
  if (allocated(gk_CSR)) deallocate(gk_CSR)
  if (allocated(gkt_CSR))deallocate(gkt_CSR)
  if (allocated(U_val)) deallocate(U_val)
  
  call MDL_FreeDataArray

end subroutine Analysis_All

!-------------------------------------------------------------------------------
! subroutine SetLoad
! 荷重ケースiについての荷重情報をセット
!-------------------------------------------------------------------------------
subroutine SetLoad(case_i)
  use BESO_DATA
  use MODEL_MOD
  implicit none
  integer, intent(in) :: case_i
  integer :: loadCase
  integer :: i_count, list_len
  integer :: i, j

  loadCase = analyCase_list(case_i,2)

  ! モデルに適用される荷重を調査
  !  設計領域の荷重節点のうち、モデルにも存在する節点をカウント
  do i =1 , 3
    i_count = 0
    do j = 1, load_data(loadCase)%Num_vec(i)
      ! このステップのモデルに存在しない節点への荷重
      if(model_nodeID(load_data(loadCase)%ID_vec(j,i))==0) cycle
      i_count = i_count + 1
    end do
    MDL_loadNum(i) = i_count
  end do

  ! 調査結果から、割り当て
  list_len = maxval(MDL_loadNum(:))
  if(allocated(MDL_loadNode)) deallocate(MDL_loadNode)
  if(allocated(MDL_loadVal))  deallocate(MDL_loadVal)
  allocate(MDL_loadNode(list_len,3), MDL_loadVal(list_len,3))
  MDL_loadNode(:,:) = 0
  MDL_loadVal(:,:)  = 0.d0

  ! モデル荷重情報に入力
  i_count = 0
  do i = 1, 3
    do j = 1, load_data(loadCase)%Num_vec(i)
      if(model_nodeID(load_data(loadCase)%ID_vec(j,i))==0) cycle
      i_count = i_count + 1
      MDL_loadNode(i_count,i) = model_nodeID(load_data(loadCase)%ID_vec(j,i))
      MDL_loadVal(i_count,i)  = load_data(loadCase)%Val_vec(j,i)
    end do
  end do

end subroutine SetLoad

!-------------------------------------------------------------------------------
! subroutine SetSupport
! 支持ケースiについての支持情報をセット
!-------------------------------------------------------------------------------
subroutine SetSupport(case_i)
  use BESO_DATA
  use MODEL_MOD
  implicit none
  integer, intent(in) :: case_i
  integer :: suppCase
  integer :: i_count, list_len
  integer :: i, j

  suppCase = analyCase_list(case_i,1)

  ! モデルに適用される荷重を調査
  !  設計領域の荷重節点のうち、モデルにも存在する節点をカウント
  do i = 1, 3
    i_count = 0
    do j = 1, NFIX_1(i)
      if(model_nodeID(NFIXP_1(j,i))==0) cycle
      i_count = i_count + 1
    end do
    nfix(i) = i_count
  end do

  ! 調査結果から、割り当て
  list_len = maxval(nfix(:))
  if(allocated(nfixp)) deallocate(nfixp)
  allocate(nfixp(list_len,3))
  nfixp(:,:) = 0

  ! モデル荷重情報に入力
  do i = 1, 3
    i_count = 0
    do j = 1, NFIX_1(i)
      if(model_nodeID(NFIXP_1(j,i))==0) cycle
      i_count = i_count + 1
      nfixp(i_count,i) = model_nodeID(NFIXP_1(j,i))
    end do
  end do
end subroutine SetSupport


!-------------------------------------------------------------------------------
! subroutine delta_energy
! 増分線形ひずみエネルギーを求める(sparce blasのテスト)
!-------------------------------------------------------------------------------
subroutine delta_energy(gk0, gk_CSR)
  use MATRIX_SOLVER
  use BESO_DATA
  use STD_CAL
  implicit none
  real(8), intent(inout) :: gk0(:), gk_CSR(:)
  real(8), dimension(lc) :: Vec_x, Vec_y
  integer :: i

  Vec_y(:) = 1.d0
  Vec_x(:) = 0.d0
  call skymat_vec_mul(lc, ind, gk0, Vec_y, Vec_x)
  Vec_y(:) = 1.d0

  print *, '増分線形ひずみエネルギー2', dot_product(Vec_y,Vec_x)

  Vec_y(:) = 1.d0
  Vec_x(:) = 0.d0
  call mkl_dcsrsymv('U', lc, gk_CSR(:), CSR_rowIndex(:), CSR_columns(:), &
  & Vec_y, Vec_x(:))
  Vec_y(:) = 1.d0

  print *, '増分線形ひずみエネルギーcsr', dot_product(Vec_y,Vec_x)

end subroutine delta_energy


!-------------------------------------------------------------------------------
! subroutine MDL_DataArray
! モデルの解析データ等を格納する配列を用意
! MDL_nodeNum,MDL_elNumの長さのデータ配列
!-------------------------------------------------------------------------------
subroutine MDL_DataArray
  use BESO_DATA, only: buckAnalyNum, BsenJoinNum
  use MODEL_MOD
  implicit none

  ! 座屈解析の求めるモード数を決定
  MDL_BsenJoinNum = BsenJoinNum
  ! 解析モード数と合成数多いほう
  eigenSolveNum = max(BsenJoinNum,buckAnalyNum)

  ! 節点データ群
  allocate(icrs (MDL_nodeNum*3)); icrs(:)=0
  allocate(force(MDL_nodeNum*3)); force(:)=0.d0
  allocate(disp (MDL_nodeNum*3)); disp(:)=0.d0
  if (eigenSolveNum/=0) then
    allocate(mode_vectors(MDL_nodeNum*3,eigenSolveNum)); mode_vectors(:,:)=0.d0
    allocate(eigen_value(eigenSolveNum)); eigen_value(:)=0.d0
  end if

  ! 要素データ群
  allocate(ep(MDL_elNum*6)); ep(:)=0.d0
  allocate(stress(MDL_elNum,6)); stress(:,:)=0.d0
  allocate(pStress(MDL_elNum,3)); pStress(:,:) = 0.d0
  allocate(pStressVec(MDL_elNum,3,3)); pStressVec(:,:,:) = 0.d0

  ! 敏感数データ群
  allocate(MDL_vonMises (MDL_elNum)); MDL_vonMises(:)=0.d0
  allocate(MDL_dispSense(MDL_elNum)); MDL_dispSense(:)=0.d0
  allocate(MDL_Daylight_fac(MDL_elNum));MDL_Daylight_fac(:)=0.0d0
  
  if (eigenSolveNum > 0) then
    allocate(MDL_buckSense_list(MDL_elNum,BsenJoinNum)); MDL_buckSense_list(:,:)=0.d0
    allocate(BsenMax(BsenJoinNum), BsenMin(BsenJoinNum), BsenMean(BsenJoinNum), BsenStd(BsenJoinNum))
  end if

end subroutine MDL_DataArray

!-------------------------------------------------------------------------------
! subroutine MDL_FreeDataArray
! モデルの解析データ等を格納する配列をメモリーから開放
! MDL_nodeNum,MDL_elNumの長さのデータ配列
!-------------------------------------------------------------------------------
subroutine MDL_FreeDataArray
  use MODEL_MOD
  use BESO_DATA, only: buckAnalyNum, BsenJoinNum
  implicit none

  ! 節点データ群
  if (allocated(icrs))  deallocate(icrs )
  if (allocated(force)) deallocate(force)
  if (allocated(disp))  deallocate(disp )
  if (allocated(mode_vectors)) deallocate(mode_vectors)
  if (allocated(eigen_value))  deallocate(eigen_value)

  ! 要素データ群
  if (allocated(ep))     deallocate(ep)
  if (allocated(stress)) deallocate(stress)
  if (allocated(pStress)) deallocate(pStress)
  if (allocated(pStressVec)) deallocate(pStressVec)

  ! 敏感数データ群
  if (allocated(MDL_vonMises))  deallocate(MDL_vonMises )
  if (allocated(MDL_dispSense)) deallocate(MDL_dispSense)
  if (allocated(MDL_Daylight_fac)) deallocate(MDL_Daylight_fac)
  if (allocated(MDL_buckSense_list)) deallocate(MDL_buckSense_list)
  if (allocated(BsenMax)) deallocate(BsenMax)
  if (allocated(BsenMin)) deallocate(BsenMin)
  if (allocated(BsenMean)) deallocate(BsenMean)
  if (allocated(BsenStd)) deallocate(BsenStd)

end subroutine MDL_FreeDataArray

!-------------------------------------------------------------------------------
! subroutine Input_Geometry
! モデルの節点座標配列,要素-節点関係配列,材料配列の生成
!-------------------------------------------------------------------------------
subroutine Input_Geometry
  use BESO_DATA
  use MODEL_MOD

  ! 1グリッド分の体積
  unitVolume = Hx*Hy*Hz

  ! 節点座標をモデル座標配列に入力
  allocate(MDL_nodePos(MDL_nodeNum,3))
  do i = 1, Nd
    if(model_nodeID(i)==0) cycle
    MDL_nodePos(model_nodeID(i),:) = Nd_in(i,:)
  end do

  ! 要素節点関係をモデル節点IDに書き換えて生成
  allocate(indv(MDL_elNum,8))
  allocate(mte(MDL_elNum))
  do i = 1, El
    if(model_elID(i)==0) cycle
    do j = 1, 8
      ! モデル節点IDに書き換え
      indv(model_elID(i),j) = model_nodeID(El_in(i,j))
    end do
    mte(model_elID(i)) = material_map(i)
  end do

end subroutine Input_Geometry

!-------------------------------------------------------------------------------
! subroutine DataToBLDArea
! モデルの解析によって得られたデータを設計領域に移動
!-------------------------------------------------------------------------------
subroutine DataToBLDArea(analyCase)
  use BESO_DATA
  use MODEL_MOD
  use daylight_factor
  implicit none
  integer, intent(in) :: analyCase
  integer :: i


  vonMises (:,analyCase) = 0.d0
  dispSense(:,analyCase) = 0.d0
  Daylight_fac_sense(:,analyCase)=0.d0
  
  if (eigenSolveNum>0) then
    buckLoad(1:MDL_BsenJoinNum,analyCase) = eigen_value(1:MDL_BsenJoinNum)
    buckSense(:,analyCase) = 0.d0
    buckSense_list(:,:,analyCase) = 0.d0
  end if

  do i = 1, El
    if(model_elID(i)==0) cycle
    vonMises (i,analyCase) = MDL_vonMises (model_elID(i))
    dispSense(i,analyCase) = MDL_dispSense(model_elID(i))
    Daylight_fac_sense(i,analyCase)=MDL_Daylight_fac(model_elID(i))
    if (eigenSolveNum > 0) then
      buckSense_list(i,:,analyCase) = MDL_buckSense_list(model_elID(i),:)
    end if
  end do

end subroutine DataToBLDArea

!-------------------------------------------------------------------------------
! subroutine FreeModel_Geometry
! モデルの節点座標配列,要素-節点関係配列,材料配列のメモリー開放
!-------------------------------------------------------------------------------
subroutine FreeModel_Geometry
  use MODEL_MOD
  if (allocated(MDL_nodePos)) deallocate(MDL_nodePos)
  if (allocated(indv)) deallocate(indv)
  if (allocated(mte))  deallocate(mte)

end subroutine FreeModel_Geometry

!-------------------------------------------------------------------------------
! subroutine GenModel_NodeID
! 設計領域 - モデル節点関係配列の作成
!-------------------------------------------------------------------------------
subroutine GenModel_NodeID
  use BESO_DATA
  use MODEL_MOD
  implicit none
  integer :: count_i
  integer :: i, j

  model_nodeID(:) = 0

  ! continuous_modelからモデル構成要素の節点にマッピング(=1)
  do i = 1, El
    ! モデル構成要素でない場合スキップ
    if(continuous_model(i)==0) cycle
    do j = 1, 8
      model_nodeID(El_in(i,j)) = 1
    end do
  end do

  ! マッピングした節点に対し、モデル節点IDを付与
  count_i = 0
  do i = 1, Nd
    ! モデル構成節点
    if (model_nodeID(i)==1) then
      count_i = count_i + 1
      model_nodeID(i) = count_i
    end if
  end do

  ! モデル節点数の決定
  MDL_nodeNum = count_i

end subroutine GenModel_NodeID

!-------------------------------------------------------------------------------
! subroutine GenModel_ElID
! 設計領域 - モデル要素関係配列の作成
!-------------------------------------------------------------------------------
subroutine GenModel_ElID
  use BESO_DATA
  use MODEL_MOD
  implicit none
  integer :: count_i
  integer :: i

  ! continuous_modelのマッピングに基づいて順番にモデル要素IDを付与
  model_elID(:) = 0
  count_i = 0
  do i = 1, El
    if(continuous_model(i)==1) then
      count_i = count_i + 1
      model_elID(i) = count_i
    end if
  end do

  ! モデル要素数の決定
  MDL_elNum = count_i

end subroutine GenModel_ElID

!=====================================================================
subroutine output_inp
  !                    解析形状のデータ出力(.inp)
  ! 出力データ内容
  ! 節点データ:節点変位
  ! 要素データ:応力、mises応力/変異?感数、座屈?感数(今は、荷重係数そのもの)
!=====================================================================
  use BESO_SUPER
  use MODEL_SUPER
  use MODEL_MOD
   implicit none

  integer :: i, k

  open(4,file='dataout/analysis/'// modelName // '_' // kstepStr // 'c' // analyCaseStr // '.inp')
  write(4,'("#",a,".inp")') modelName
  write(4,'("1")')
  write(4,'("data_geom")')

  write(4,'("step1",i3)')

  write(4,'(2i7)') MDL_nodeNum, MDL_elNum

  do i=1,MDL_nodeNum  !節点数 節点の座標
    write(4,'(1x,i7,3E15.7)') i,MDL_nodePos(i,1:3)
  end do

  do i=1,MDL_elNum  !要素数 要素の節点番号
    write(4,'(i7,i5 ,"    hex ",8i10)') i, mte(i) ,indv(i,1),indv(i,2),indv(i,3),indv(i,4),indv(i,5),indv(i,6),indv(i,7),indv(i,8)
  end do

  write(4,'(2(I2,1X))') 3, MDL_BsenJoinNum+3

  ! -----節点データ-----
  !データ構造
  write(4,'("3 1 1 1")')

  !データラベル
  write(4,'("u,"/,"v,"/,"w,")')

  !データ
  do i=1,MDL_nodeNum
    write(4,'(i7,12E15.7)') i,disp(3*(i-1)+1), disp(3*(i-1)+2), disp(3*(i-1)+3)
  end do

  !-----要素データ-----
  ! データ構造
  write(4,'(I2,1X)', advance='no') 3 + MDL_BsenJoinNum
  do i = 1, MDL_BsenJoinNum+3
    write(4,'(I2,1X)', advance='no') 1
  end do
  write(4,*)

  ! データラベル
  write(4,'("VonMises,"/," Disp_sens,"/,"Daylight_sens,")')
  do i = 1, MDL_BsenJoinNum
    write(4,'("Buck_sens", I2.2, ",")') i
  end do

  ! データ
  do i=1,MDL_elNum
    if (not(MDL_BsenJoinNum==0)) then
      write(4,'(I7,1X,<MDL_BsenJoinNum+3>(E15.7,1X))') i, MDL_vonMises(i), MDL_dispSense(i),MDL_Daylight_fac(i), MDL_buckSense_list(i,:)
    else
      write(4,'(I7,1X,2(E15.7,1X))') i, MDL_vonMises(i), MDL_dispSense(i)
    end if
  end do

  close(4)

end subroutine output_inp

!=====================================================================
! subroutine OutputGeometory_inp
!                    解析形状のデータ出力(.inp)
! 出力データ内容
! 節点データ:節点変位
! 要素データ:応力、mises応力/変異?感数、座屈?感数(今は、荷重係数そのもの)
!=====================================================================
subroutine OutputGeometory_inp(file_name)
  use BESO_SUPER
  use MODEL_MOD
   implicit none
  character(*), intent(in) :: file_name
  integer :: i, k

  open(4,file='dataout/analysis/'// file_name // '.inp')
  write(4,'("#",a,".inp")') file_name
  write(4,'("1")')
  write(4,'("data_geom")')

  write(4,'("step1",i3)')

  write(4,'(2i7)') MDL_nodeNum, MDL_elNum

  do i=1,MDL_nodeNum  !節点数 節点の座標
    write(4,'(1x,i7,3f15.7)') i,MDL_nodePos(i,1),MDL_nodePos(i,2),MDL_nodePos(i,3)
  end do

  do i=1,MDL_elNum  !要素数 要素の節点番号
    write(4,'(i7,i5 ,"    hex ",8i10)') i, mte(i) ,indv(i,1),indv(i,2),indv(i,3),indv(i,4),indv(i,5),indv(i,6),indv(i,7),indv(i,8)
  end do

  write(4,'("1  1")')

  write(4,'("1 1"/,"dummy,")')
  do i=1,MDL_nodeNum  !節点数 節点変位
    write(4,'(i7,e25.10)') i, 1.d0
  end do

  ! それぞれのデータラベル
  write(4,'("1 1")')
  write(4,'("dummy,")')
  do i=1,MDL_elNum
    write(4,'(i7,e25.10)') i, 1.d0
  end do

  close(4)
end subroutine OutputGeometory_inp

!=====================================================================
subroutine OutputMode_inp
  !                    解析形状のデータ出力(.inp)
  ! 出力データ内容
  ! 節点データ:節点変位
  ! 要素データ:応力、mises応力/変異?感数、座屈?感数(今は、荷重係数そのもの)
!=====================================================================
  use BESO_DATA
  use BESO_SUPER
  use MODEL_MOD
   implicit none

  real(8) :: vol_mode

  integer :: i, j, k
  open(4,file='dataout/analysis/'// modelName // '_' // kstepStr // 'c' // analyCaseStr // 'mode.inp')
  write(4,'("#",a,".inp")') modelName
  write(4,'(i5)') eigenSolveNum
  write(4,'("data_geom")')

  do j = 1, eigenSolveNum
    write(4,'("step",i1)') j

    write(4,'(2i7)') MDL_nodeNum,MDL_elNum

    vol_mode = maxval([Hx, Hy, Hz]) * 5.d0/ maxval(abs(mode_vectors(:,j)))
    do i=1,MDL_nodeNum  !節点数 節点の座標
      write(4,'(1x,i7,3f15.7)') i,MDL_nodePos(i,1) + vol_mode * mode_vectors(3*(i-1)+1,j),&
      &MDL_nodePos(i,2) + vol_mode * mode_vectors(3*(i-1)+2,j),MDL_nodePos(i,3) + vol_mode * mode_vectors(3*(i-1)+3,j)
    end do

    do i=1,MDL_elNum  !要素数 要素の節点番号
      write(4,'(i7, i5,"    hex ",8i10)') i,mte(i),indv(i,1),indv(i,2),indv(i,3),indv(i,4),indv(i,5),indv(i,6),indv(i,7),indv(i,8)
    end do

    write(4,'(2I3)') 6, MDL_BsenJoinNum + 8

    write(4,'("6 1 1 1 1 1 1"/,"u,"/,"v,"/,"w,"/,"eig_x,"/,"eig_y,"/,"eig_z,")')
    do i=1,MDL_nodeNum  !節点数 節点変位
      write(4,'(i7,6e25.10)') i,(disp(3*(i-1)+k)*1.0d+3, k = 1,3),&
      & (mode_vectors(3*(i-1)+k,j), k = 1,3)
    end do

    write(4,'(i4)', advance='no') MDL_BsenJoinNum + 8
    do i = 1, MDL_BsenJoinNum + 8
      write(4,'(i4)', advance='no') 1
    end do
    write(4,*)

    write(4,'("9 1 1 1 1 1 1 1 1 1")')
    write(4,'(" sigma-x,"/," sigma-y,"/," sigma-z,"/," sigma-xy,"/," sigma-xz,"/," sigma-yz,"/," VonMises,"/," Disp_sens,"/," Buck_sens,")')
    do i=1,MDL_elNum
      write(4,'(i7,<MDL_BsenJoinNum+8>e25.10)') i,(stress(i,k), k=1,6), MDL_vonMises(i), MDL_dispSense(i), MDL_buckSense_list(i,1:MDL_BsenJoinNum)
    end do
  end do

  close(4)

end subroutine OutputMode_inp

!=====================================================================
subroutine OutputMode_inp2
!解析形状のデータ出力(.inp)
!=====================================================================
  use BESO_SUPER
  use MODEL_SUPER
  use MODEL_MOD
  implicit none

  integer :: i, j, k
  real(8), allocatable :: node_mode(:)

  allocate(node_mode(eigenSolveNum*3))

  open(4,file='dataout/analysis/'// modelName // '_' // kstepStr // 'c' // analyCaseStr // 'mode2.inp')
  write(4,'("#",a,".inp")') modelName
  write(4,'("1")')
  write(4,'("data_geom")')

  write(4,'("step1",i3)')

  write(4,'(2i7)') MDL_nodeNum, MDL_elNum
  do i=1,MDL_nodeNum  !節点数 節点の座標
    write(4,'(1x,i7,3E15.7)') i,MDL_nodePos(i,1),MDL_nodePos(i,2),MDL_nodePos(i,3)
  end do

  do i=1,MDL_elNum  !要素数 要素の節点番号
    write(4,'(i7,i5 ,"    hex ",8i10)') i, mte(i) ,indv(i,1),indv(i,2),indv(i,3),indv(i,4),indv(i,5),indv(i,6),indv(i,7),indv(i,8)
  end do

  write(4,'(2(I4,2X))') 3*eigenSolveNum, 18

  ! -----節点データ-----
  !データ構造
  write(4,'(i4)', advance='no') 3*eigenSolveNum
  do i = 1, 3*eigenSolveNum
    write(4,'(i4)', advance='no') 1
  end do
  write(4,*)

  !データラベル
  do i = 1, eigenSolveNum
    write(4,'("u",i2.2,","/,"v",i2.2,","/,"w",i2.2,",")') i, i, i
  end do

  !データ
  do i=1,MDL_nodeNum
    do j = 1, eigenSolveNum
      do k = 1, 3
        node_mode((j-1)*3+k) = mode_vectors((i-1)*3+k,j)
      end do
    end do
    write(4,'(1x,i7,<eigenSolveNum*3>E15.7)') i, node_mode(:)
  end do

  !-----要素データ-----
  ! データ構造
  write(4,'("18 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1")')

  ! データラベル
  write(4,'(" sigma-x,"/," sigma-y,"/," sigma-z,"/," sigma-xy,"/," sigma-xz,"/," sigma-yz,"/," p1,"/," p2,"/," p3,"/,"p1u,"/,"p1v,"/,"p1w,"/,"p2u,"/,"p2v,"/,"p2w,"/,"p3u,"/,"p3v,"/,"p3w,")')

  ! データ
  do i=1,MDL_elNum
    write(4,'(i7,21E15.7)') i,stress(i,:), pStress(i,:), pStressVec(i,1,1:3), pStressVec(i,2,1:3), pStressVec(i,3,1:3)
  end do
  
  close(4)
  
end subroutine OutputMode_inp2

!=====================================================================
subroutine OutputBuild_inp
!                    設計領域上のデータ出力(.inp)
! 出力データ内容
! 節点データ:なし
! 要素データ:敏感数(今は、荷重係数そのもの)
!=====================================================================
use BESO_SUPER
use BESO_DATA
 implicit none

  integer :: i, j

  open(4,file='dataout/build_domain/' // modelName // '_' // kstepStr // 'build.inp')
  write(4,'("#",a,".inp")') modelName
  write(4,'("1")')
  write(4,'("data_geom")')

  write(4,'("step1",i3)')

  write(4,'(2i7)') Nd,El

  do i=1,Nd  !節点数 節点の座標
    write(4,'(1x,i7,3E15.7)') i,Nd_in(i,:)
  end do

  do i=1,El  !要素数 要素の節点番号
    write(4,'(i7,i5,"    hex ",8i10)') i,Cre_Map(i),El_in(i,:)
  end do

  ! 節点のデータ数、要素のデータ数
  write(4,'(2(I10,1X))') 1, BsenJoinNum*totalAnalyCase + 3

  ! 節点データ
  write(4,'("  1  1")')
  write(4,'("Sense Node,")')
  do i=1,Nd  !節点数 節点変位
    write(4,'(i7,E15.7)') i, Nd_sen(i)
  end do

  ! 要素データ構造
  write(4,'(I2,1X,<BsenJoinNum*totalAnalyCase+3>("1",1X))',advance='no') BsenJoinNum + 3
  write(4,*)

  ! 要素データラベル
  do i = 1, totalAnalyCase
    do j = 1, BsenJoinNum
      write(4,'("BuckSense", I2.2, "C", I2.2, ",")') j, i
    end do
  end do
  write(4,'(" Sense Origin,"/,"Sense Filtered,"/,"Be,")')

  ! 要素データ値
  do i=1,El
    write(4,'(i7,<BsenJoinNum*totalAnalyCase+2>(1X,E15.7), i7)') i, (buckSense_list(i,1:BsenJoinNum,j),j=1,totalAnalyCase), Sensitivity(i), El_sen(i), Be(i)
  end do

  close(4)

end subroutine OutputBuild_inp


!-------------------------------------------------------------------------------
! subroutine output_analyInfo
! 解析ケースごとの解析・敏感数算出結果を出力
! 160605 : @yamazaki 荷重ケースごとにファイルを分けた。ステップ経過が追えるように。
!-------------------------------------------------------------------------------
subroutine output_analyInfo
  use BESO_SUPER, only: kstep
  use MODEL_MOD
  implicit none
  integer :: i

  if (kstep == 1) then
    open(300, file='./dataout/analysis/analyInfo-Case' // analyCaseStr // '.txt')
    write(300,'(12(A,1X))', advance='no') 'step', 'inner-E', 'outer-E', 'buck-load', &
                 & 'MisesMean', 'MisesStd', 'MisesMax', 'MisesMin', &
                 & 'dispMean', 'dispStd', 'dispMax', 'dispMin'
    do i = 1, MDL_BsenJoinNum
      write(300,'(<MDL_BsenJoinNum>(A,I2.2,1X))',advance='no') 'buckMean', i
    end do
    do i = 1, MDL_BsenJoinNum
      write(300,'(<MDL_BsenJoinNum>(A,I2.2,1X))',advance='no') 'buckStd', i
    end do
    do i = 1, MDL_BsenJoinNum
      write(300,'(<MDL_BsenJoinNum>(A,I2.2,1X))',advance='no') 'buckMax', i
    end do
    do i = 1, MDL_BsenJoinNum
      write(300,'(<MDL_BsenJoinNum>(A,I2.2,1X))',advance='no') 'buckMin', i
    end do
    write(300,*)
  else
    open(300, file='./dataout/analysis/analyInfo-Case' // analyCaseStr // '.txt', position='append')
  end if

  if (not(MDL_BsenJoinNum==0)) then
    write(300, '(I4, 1X, <MDL_BsenJoinNum*4 + 11>(E15.7,1X))') kstep, V_energyCase_i, U_energyCase_i, eigen_value(1), &
    & vonMean, vonStd, vonMax, vonMin, &
    & DsenMean, DsenStd, DsenMax, DsenMin, &
    & BsenMean(1:MDL_BsenJoinNum), BsenStd(1:MDL_BsenJoinNum), BsenMax(1:MDL_BsenJoinNum), BsenMin(1:MDL_BsenJoinNum)
  else
    write(300, '(I4, 1X, 10(E15.7,1X))') kstep, V_energyCase_i, U_energyCase_i, &
    & vonMean, vonStd, vonMax, vonMin, &
    & DsenMean, DsenStd, DsenMax, DsenMin
  end if

  close(300)

end subroutine output_analyInfo

!-------------------------------------------------------------------------------
! subroutine Output_eigenInfo
! 固有値の出力
! 160610 : @yamazaki 固有値だけ分離して出力
!-------------------------------------------------------------------------------
subroutine output_eigenInfo
  use BESO_DATA
  use BESO_SUPER
  use MODEL_MOD
  implicit none

  integer :: i
  character mode*2

  if (kstep == 1) then
    open(300, file='./dataout/analysis/eigenInfo-Case' // analyCaseStr // '.txt')
    do i = 1, eigenSolveNum
      write(mode,'(I2.2)') i
      write(300,353, advance='no')  'mode' // mode
    end do
    write(300, *)
  else
    open(300, file='./dataout/analysis/eigenInfo-Case' // analyCaseStr // '.txt', position='append')
  end if

  do i = 1, eigenSolveNum
    write(300, 354, advance='no') eigen_value(i)
  end do

  close(300)
353 format(A26)
354 format(E26.15)
end subroutine output_eigenInfo

!-------------------------------------------------------------------------------
! sobroutine output_daylight_factor
!昼光率分布データ生成
!-------------------------------------------------------------------------------
subroutine output_daylight_factor
  use BESO_SUPER
  use MODEL_SUPER
  use MODEL_MOD
  use daylight_factor
  implicit none 
  
  integer ::i,j,k,l,full_ind
  real(8),dimension(pnum,2) :: full_r
  integer,dimension(elnum,4) :: el_nod
  
  
  open(5,file='dataout/daylight_factor/' // modelName // '_' // kstepStr // 'daylight.inp')
  write(5,'("#",a,".inp")') modelName
  write(5,'("1")')
  write(5,'("data_geom")')
  write(5,'("step1",i3)')
  write(5,'(2i7)')pnum,elnum
  !節点座標-----------------------------------------------------------------------
  i=0
  j=0
  k=0
  l=0
  do i=0,sy,searchy
    do j=1,pnum/numx/numy
        do k=0,sx,searchx
            l=l+1
            full_r(j,1)=k
            full_r(j,2)=i
            write(5,'(1x,i7,3e15.7)')l,full_r(j,1),full_r(j,2),0
        end do
    end do
  end do
  !要素構成接点-------------------------------------------------------------------
  i=0
  j=0
  k=0
  l=0
  do i=1,numy
      do j=1,numx
        l=l+1
        full_ind=(i-1)*numx+j
        el_nod(full_ind,1)=l
        el_nod(full_ind,2)=l+1
        el_nod(full_ind,3)=l+(numx+1)+1
        el_nod(full_ind,4)=l+(numx+1)
      end do
    l=l+1
  end do
  
  do k=1,elnum
      write(5,'(i7,"   0   quad   ",4i15)')k,el_nod(k,1),el_nod(k,2),el_nod(k,3),el_nod(k,4)
  end do
  
  write(5,'("0   1")')
  write(5,'("1   1")')
  write(5,'("Daylight_Factor,")')
     

  j=0
  do j=1,elnum
      write(5,'(i7,2x,1(e15.8,2x))')j,U_val(j)
  end do

  close(5)
end subroutine output_daylight_factor

subroutine output_varience_val
  use BESO_SUPER
  use MODEL_SUPER
  use MODEL_MOD
  use daylight_factor
  implicit none
  
  integer :: i
  real(8) :: varience_sum
  varience_sum=0
  open(92,file='dataout/daylight_factor/' // modelName // '_' // kstepStr // 'varienceval.txt')
  do i=1,MDL_elNum
      write(92,'("分散",i0,e10.3)')i,1/MDL_Daylight_fac(i)
      varience_sum=varience_sum+1/MDL_Daylight_fac(i)
  end do
  write(92,'(e10.3)')varience_sum
  close(92)
  
end subroutine output_varience_val


end module ANALYSIS_CTRL
