    module daylight_factor
    use BESO_DATA
    use FILE_IO
    use MODEL_MOD
    implicit none
 
    real(8),allocatable :: U_val(:),DF_val(:)
    real(8) :: pt_list(3),tpt_list(4,2),Dist,Distxy,alpha,area_val,variance_val
	integer ::sx,sy,searchx,searchy,wh,t,n,numx,numy,pnum,numb,elnum
    real(8) :: winpt_list(4,3)
    integer :: num
    real(8) ::sinxy,cosxy
    real(8) :: PU_val
    real(8) :: ave_val,p_variance_val
    
    contains
    
    
    subroutine potential_daylight_factor
    implicit none
    integer :: i,k,q
    !slab X方向　Y方向のスラブの長さw 壁の高さ
	sx=Xn*100
    !syは現状壁の高さと同じ値に設定
	sy=Zn*100
    wh=Zn*100
    
	!床探索要素のサイズ
	searchx=10
	searchy=10
    !分割数
    numx=sx/searchx
    numy=sy/searchy
    elnum=numx*numy
    
    !節点数
    pnum=(numx+1)*(numy+1)
    allocate(U_val(pnum))
    
    !allocate(MDL_Daylight_fac(MDL_elNum))
    
    numb=0
    winpt_list(:,:)=0.0d0
    variance_val=0
    
!=======================================================================================================================
    open(91,file='./data out/potential_out.txt',action='write')
    num=0    
    do i=1,MDL_elNum
           num=num+1
           
           !!winpt_list：窓の頂点座標　1右下　2左下　3左上　4右上
           winpt_list(1,1)=Nd_in(El_in(i,2),1)*100
           winpt_list(2,1)=Nd_in(El_in(i,1),1)*100
           winpt_list(3,1)=Nd_in(El_in(i,5),1)*100
           winpt_list(4,1)=Nd_in(El_in(i,6),1)*100
           
           winpt_list(1,3)=Nd_in(El_in(i,2),3)*100
           winpt_list(2,3)=Nd_in(El_in(i,1),3)*100
           winpt_list(3,3)=Nd_in(El_in(i,5),3)*100
           winpt_list(4,3)=Nd_in(El_in(i,6),3)*100
           
           winpt_list(1,2)=0
           winpt_list(2,2)=0
           winpt_list(3,2)=0
           winpt_list(4,2)=0
        write(91,'("番号",i0)')num
        write(91,*)"要素の座標　 頂点位置　  x     y    z "
        write(91,'(1f10.3,3f10.5)')1,winpt_list(1,1),winpt_list(1,2),winpt_list(1,3)
        write(91,'(1f10.3,3f10.5)')2,winpt_list(2,1),winpt_list(2,2),winpt_list(2,3)
        write(91,'(1f10.3,3f10.5)')3,winpt_list(3,1),winpt_list(3,2),winpt_list(3,3)
        write(91,'(1f10.3,3f10.5)')4,winpt_list(4,1),winpt_list(4,2),winpt_list(4,3)
     PU_val=0
     U_val(:)=0
     variance_val=0
     p_variance_val=0
     do k=searchy/2,(sy-searchy/2),searchy
            do q=searchx/2,(sx-searchx/2),searchx
            pt_list(1)=q
            pt_list(2)=k
            pt_list(3)=0
            num=num+1
             do t=1,4
             !開口の節点から探索点までの直線距離
             Dist=((winpt_list(t,1)-pt_list(1))**2+(winpt_list(t,2)-pt_list(2))**2+(winpt_list(t,3)-pt_list(3))**2)**0.5
             !write(98,*)"直線距離",Dist
             !
             Distxy=((winpt_list(t,1)-pt_list(1))**2+(winpt_list(t,2)-pt_list(2))**2)**0.5
             !write(98,*)Distxy
             alpha=Distxy/Dist
        
             sinxy=(winpt_list(t,1)-pt_list(1))/Distxy
             tpt_list(t,1)=pt_list(1)+alpha*sinxy
        
             cosxy=abs(winpt_list(t,2)-pt_list(2))/Distxy
        
             tpt_list(t,2)=pt_list(2)-alpha*cosxy
             !write(91,'(1i1,3f10.4,3f10.4)') t,tpt_list(t,1),tpt_list(t,2)
             end do
        area_val=abs(tpt_list(1,1)*tpt_list(2,2)+tpt_list(2,1)*tpt_list(3,2)+tpt_list(3,1)*tpt_list(4,2)&
              &+tpt_list(4,1)*tpt_list(1,2)-(tpt_list(2,1)*tpt_list(1,2)+tpt_list(3,1)*tpt_list(2,2)&
              &+tpt_list(4,1)*tpt_list(3,2)+tpt_list(1,1)*tpt_list(4,2)))/2
        !write(98,*)"立体投射角面積"
       ! write(91,'(3f10.4)')area_val
        U_val(num)=area_val/pi*100
        !write(91,*)"立体投射率(％)"
        !write(91,'(3f10.4)')U_val(num)
        PU_val=PU_val+U_val(num)
        write(91,'("PU",i0,e10.3)')num,PU_val
        
        end do
     end do
     t=0
    ave_val=PU_val/pnum
    p_variance_val=0

    do t=1,pnum
       p_variance_val=(U_val(t)-ave_val)**2+p_variance_val
    end do
       variance_val=(1/(p_variance_val/pnum))**0.5
    !open(92,file='./data out/varience_out.txt',action='write')
   ! write(92,'("分散",i0,e10.3)')elnum,variance_val
    MDL_Daylight_fac(i)=1/variance_val
    !numb=numb+1
     !call daylight_model(sx,sy,searchx,searchy,elnum,pnum,numx,numy,U_val,numb)
    
    
    
    t=0
    num=0
    end do
    !close(92)
    
     close(91) 
    end subroutine
!======================================================================
    !各床要素における昼光率の合計値を算出
!======================================================================
    subroutine cal_daylight_factor
    implicit none
    integer :: i,k,q
    real(8) :: PU
    numb=0
    winpt_list(:,:)=0.0d0
    num=0
    
    
    open(6,file='./data out/check_out.txt',action='write')
    do i=1,MDL_elNum
           if (Be(i)==1) cycle
           !U_val(:)=0
           !!winpt_list：窓の頂点座標　1右下　2左下　3左上　4右上
           winpt_list(1,1)=Nd_in(El_in(i,2),1)*100
           winpt_list(2,1)=Nd_in(El_in(i,1),1)*100
           winpt_list(3,1)=Nd_in(El_in(i,5),1)*100
           winpt_list(4,1)=Nd_in(El_in(i,6),1)*100
           
           winpt_list(1,3)=Nd_in(El_in(i,2),3)*100
           winpt_list(2,3)=Nd_in(El_in(i,1),3)*100
           winpt_list(3,3)=Nd_in(El_in(i,5),3)*100
           winpt_list(4,3)=Nd_in(El_in(i,6),3)*100
           
           winpt_list(1,2)=0
           winpt_list(2,2)=0
           winpt_list(3,2)=0
           winpt_list(4,2)=0
     num=0
     PU=0
     do k=searchy/2,(sy-searchy/2),searchy
            do q=searchx/2,(sx-searchx/2),searchx
            pt_list(1)=q
            pt_list(2)=k
            pt_list(3)=0
            num=num+1
             do t=1,4
             !開口の節点から探索点までの直線距離
             Dist=((winpt_list(t,1)-pt_list(1))**2+(winpt_list(t,2)-pt_list(2))**2+(winpt_list(t,3)-pt_list(3))**2)**0.5
             !write(98,*)"直線距離",Dist
             !
             Distxy=((winpt_list(t,1)-pt_list(1))**2+(winpt_list(t,2)-pt_list(2))**2)**0.5
             !write(98,*)Distxy
             alpha=Distxy/Dist
        
             sinxy=(winpt_list(t,1)-pt_list(1))/Distxy
             tpt_list(t,1)=pt_list(1)+alpha*sinxy
        
             cosxy=abs(winpt_list(t,2)-pt_list(2))/Distxy
        
             tpt_list(t,2)=pt_list(2)-alpha*cosxy
             !write(91,'(1i1,3f10.4,3f10.4)') t,tpt_list(t,1),tpt_list(t,2)
             end do
        area_val=abs(tpt_list(1,1)*tpt_list(2,2)+tpt_list(2,1)*tpt_list(3,2)+tpt_list(3,1)*tpt_list(4,2)&
              &+tpt_list(4,1)*tpt_list(1,2)-(tpt_list(2,1)*tpt_list(1,2)+tpt_list(3,1)*tpt_list(2,2)&
              &+tpt_list(4,1)*tpt_list(3,2)+tpt_list(1,1)*tpt_list(4,2)))/2
        !write(98,*)"立体投射角面積"
       ! write(91,'(3f10.4)')area_val
        PU=area_val/pi*100
        
        U_val(num)=U_val(num)+PU
        write(6,'("PU",i0,e10.3)')num,U_val(num)
        end do 
     end do
    end do
    close(6)
    end subroutine 
    end module
!---------------------------------------------------------------------------------------------------------------------------------------------
    !モデル化部分
    subroutine daylight_model(slabx,slaby,searchx,searchy,elnum,pnum,numx,numy,U_val,number)
    implicit none
    integer :: t,k,n,i,j,p,ky,kx,nd_ind,full_ind,ix,iy,pn,jnod,num,ii,iii
    integer,intent(in) :: slabx,slaby,searchx,searchy,pnum,numx,numy,number,elnum

    real(8),dimension(elnum),intent(in) :: U_val
    
    real(8),dimension(pnum,2) :: full_r
    integer,dimension(elnum,4) :: el_nod

    character nameprth*9,name1*4
    
    write(name1,fmt='(I4.4)') number
    nameprth='data out\' 
     open(10,file=nameprth//name1//'_full.inp')
     write(10,'("#",a,".inp")')name1
     write(10,'("1")')
     write(10,'("data_geom")')
     write(10,'(a)') "step1"
     write(10,'(2i7)')pnum,elnum

    !節点座標==========================================================
    num=0
     do iy=0,slaby,searchy
       do pn=1,pnum/numx/numy
         do ix=0,slabx,searchx
            num=num+1
            full_r(pn,1)=ix
            full_r(pn,2)=iy
            write(10,'(1x,i7,3e15.7)')num,full_r(pn,1),full_r(pn,2),0
         end do
        end do
       end do

     !=要素構成節点=====================================================
     nd_ind=0
     do ky=1,numy
       do kx=1,numx
          nd_ind=nd_ind+1
          full_ind=(ky-1)*numx+kx
          el_nod(full_ind,1)=nd_ind
          el_nod(full_ind,2)=nd_ind+1
          el_nod(full_ind,3)=nd_ind+(numx+1)+1
          el_nod(full_ind,4)=nd_ind+(numx+1)
       end do
         nd_ind=nd_ind+1
     end do

!=昼光率の平均算出=================================================

!nd_ind=0
!do ky=1,numy
 !   do kx=1,numx
  !      nd_ind=nd_ind+1
   !     full_ind=(ky-1)*numx+kx
   !    TU_val(full_ind)=(TU_val(nd_ind)+TU_val(nd_ind+1)+TU_val(nd_ind+(numx+1)+1)+TU_val(nd_ind+(numx+1)))
  !  end do
  !  nd_ind=nd_ind+1
!end do

     !=inpファイル作成==================================================
      do num=1,elnum
        write(10,'(i7,"   0   quad   ",4i15)')num,el_nod(num,1),el_nod(num,2),el_nod(num,3),el_nod(num,4)
     end do


      write(10,'("0   1")')

      write(10,'("1   1")')
      j=0
      write(10,'("Daylight_Factor,")')
      do j=1,elnum
         write(10,'(i7,2x,1(e15.8,2x))')j,U_val(j)
      end do

    end subroutine
!---------------------------------------------------------------------------------------------------------------------------------
  