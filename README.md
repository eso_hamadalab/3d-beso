# memo #
17/02/22 add new parameter "topo_rate" to "BESO_PARAM"  
topo_rate :  
topo_rate < 0.d0 :: no topology movement rate constraint  
topo_rate >= 0.d0 :: topology movement rate is limited  
